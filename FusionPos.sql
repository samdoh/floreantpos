-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 14, 2018 at 07:45 WB
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `FusionPos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ACTION_HISTORY`
--

CREATE TABLE `ACTION_HISTORY` (
  `ID` int(11) NOT NULL,
  `ACTION_TIME` datetime DEFAULT NULL,
  `ACTION_NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ACTION_HISTORY`
--

INSERT INTO `ACTION_HISTORY` (`ID`, `ACTION_TIME`, `ACTION_NAME`, `DESCRIPTION`, `USER_ID`) VALUES
(1, '2018-08-11 14:45:55', 'NEW CHECK', 'CHK#: :2', 4),
(2, '2018-08-11 14:46:06', 'EDIT CHECK', 'CHK#: :2', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ATTENDENCE_HISTORY`
--

CREATE TABLE `ATTENDENCE_HISTORY` (
  `ID` int(11) NOT NULL,
  `CLOCK_IN_TIME` datetime DEFAULT NULL,
  `CLOCK_OUT_TIME` datetime DEFAULT NULL,
  `CLOCK_IN_HOUR` smallint(6) DEFAULT NULL,
  `CLOCK_OUT_HOUR` smallint(6) DEFAULT NULL,
  `CLOCKED_OUT` bit(1) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `SHIFT_ID` int(11) DEFAULT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ATTENDENCE_HISTORY`
--

INSERT INTO `ATTENDENCE_HISTORY` (`ID`, `CLOCK_IN_TIME`, `CLOCK_OUT_TIME`, `CLOCK_IN_HOUR`, `CLOCK_OUT_HOUR`, `CLOCKED_OUT`, `USER_ID`, `SHIFT_ID`, `TERMINAL_ID`) VALUES
(1, '2018-08-11 14:43:27', NULL, 14, NULL, b'0', 3, 1, 7130),
(2, '2018-08-11 14:44:46', NULL, 14, NULL, b'0', 1, 1, 7130),
(3, '2018-08-11 14:45:47', NULL, 14, NULL, b'0', 4, 1, 7130);

-- --------------------------------------------------------

--
-- Table structure for table `CASH_DRAWER`
--

CREATE TABLE `CASH_DRAWER` (
  `ID` int(11) NOT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CASH_DRAWER_RESET_HISTORY`
--

CREATE TABLE `CASH_DRAWER_RESET_HISTORY` (
  `ID` int(11) NOT NULL,
  `RESET_TIME` datetime DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `COOKING_INSTRUCTION`
--

CREATE TABLE `COOKING_INSTRUCTION` (
  `ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `COUPON_AND_DISCOUNT`
--

CREATE TABLE `COUPON_AND_DISCOUNT` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(120) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `BARCODE` varchar(120) DEFAULT NULL,
  `QUALIFICATION_TYPE` int(11) DEFAULT NULL,
  `APPLY_TO_ALL` bit(1) DEFAULT NULL,
  `MINIMUM_BUY` int(11) DEFAULT NULL,
  `MAXIMUM_OFF` int(11) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `EXPIRY_DATE` datetime DEFAULT NULL,
  `ENABLED` bit(1) DEFAULT NULL,
  `AUTO_APPLY` bit(1) DEFAULT NULL,
  `MODIFIABLE` bit(1) DEFAULT NULL,
  `NEVER_EXPIRE` bit(1) DEFAULT NULL,
  `UUID` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `COUPON_AND_DISCOUNT`
--

INSERT INTO `COUPON_AND_DISCOUNT` (`ID`, `NAME`, `TYPE`, `BARCODE`, `QUALIFICATION_TYPE`, `APPLY_TO_ALL`, `MINIMUM_BUY`, `MAXIMUM_OFF`, `VALUE`, `EXPIRY_DATE`, `ENABLED`, `AUTO_APPLY`, `MODIFIABLE`, `NEVER_EXPIRE`, `UUID`) VALUES
(1, 'Buy 1 and get 1 free', 1, NULL, 0, b'1', 2, 0, 100, NULL, b'1', b'0', b'0', b'1', NULL),
(2, 'Buy 2 and get 1 free', 1, NULL, 0, b'1', 3, 0, 100, NULL, b'1', b'1', b'0', b'1', NULL),
(3, '10% Off', 1, NULL, 0, b'1', 1, 0, 10, NULL, b'1', b'0', b'0', b'1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `CURRENCY`
--

CREATE TABLE `CURRENCY` (
  `ID` int(11) NOT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  `NAME` varchar(30) DEFAULT NULL,
  `SYMBOL` varchar(10) DEFAULT NULL,
  `EXCHANGE_RATE` double DEFAULT NULL,
  `TOLERANCE` double DEFAULT NULL,
  `BUY_PRICE` double DEFAULT NULL,
  `SALES_PRICE` double DEFAULT NULL,
  `MAIN` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `CURRENCY`
--

INSERT INTO `CURRENCY` (`ID`, `CODE`, `NAME`, `SYMBOL`, `EXCHANGE_RATE`, `TOLERANCE`, `BUY_PRICE`, `SALES_PRICE`, `MAIN`) VALUES
(6, 'Ksh.', 'KSHS', 'Ksh.', 1, 0, 0, 0, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `CURRENCY_BALANCE`
--

CREATE TABLE `CURRENCY_BALANCE` (
  `ID` int(11) NOT NULL,
  `BALANCE` double DEFAULT NULL,
  `CURRENCY_ID` int(11) DEFAULT NULL,
  `CASH_DRAWER_ID` int(11) DEFAULT NULL,
  `DPR_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CUSTOMER`
--

CREATE TABLE `CUSTOMER` (
  `AUTO_ID` int(11) NOT NULL,
  `LOYALTY_NO` varchar(30) DEFAULT NULL,
  `LOYALTY_POINT` int(11) DEFAULT NULL,
  `SOCIAL_SECURITY_NUMBER` varchar(60) DEFAULT NULL,
  `PICTURE` blob,
  `HOMEPHONE_NO` varchar(30) DEFAULT NULL,
  `MOBILE_NO` varchar(30) DEFAULT NULL,
  `WORKPHONE_NO` varchar(30) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `SALUTATION` varchar(60) DEFAULT NULL,
  `FIRST_NAME` varchar(60) DEFAULT NULL,
  `LAST_NAME` varchar(60) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `DOB` varchar(16) DEFAULT NULL,
  `SSN` varchar(30) DEFAULT NULL,
  `ADDRESS` varchar(220) DEFAULT NULL,
  `CITY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `ZIP_CODE` varchar(10) DEFAULT NULL,
  `COUNTRY` varchar(30) DEFAULT NULL,
  `VIP` bit(1) DEFAULT NULL,
  `CREDIT_LIMIT` double DEFAULT NULL,
  `CREDIT_SPENT` double DEFAULT NULL,
  `CREDIT_CARD_NO` varchar(30) DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CUSTOMER_PROPERTIES`
--

CREATE TABLE `CUSTOMER_PROPERTIES` (
  `id` int(11) NOT NULL,
  `property_value` varchar(255) DEFAULT NULL,
  `property_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CUSTOM_PAYMENT`
--

CREATE TABLE `CUSTOM_PAYMENT` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `REQUIRED_REF_NUMBER` bit(1) DEFAULT NULL,
  `REF_NUMBER_FIELD_NAME` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DATA_UPDATE_INFO`
--

CREATE TABLE `DATA_UPDATE_INFO` (
  `ID` int(11) NOT NULL,
  `LAST_UPDATE_TIME` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `DATA_UPDATE_INFO`
--

INSERT INTO `DATA_UPDATE_INFO` (`ID`, `LAST_UPDATE_TIME`) VALUES
(1, '2018-08-11 14:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `DELIVERY_ADDRESS`
--

CREATE TABLE `DELIVERY_ADDRESS` (
  `ID` int(11) NOT NULL,
  `ADDRESS` text,
  `PHONE_EXTENSION` varchar(10) DEFAULT NULL,
  `ROOM_NO` varchar(30) DEFAULT NULL,
  `DISTANCE` double DEFAULT NULL,
  `CUSTOMER_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DELIVERY_CHARGE`
--

CREATE TABLE `DELIVERY_CHARGE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(220) DEFAULT NULL,
  `ZIP_CODE` varchar(20) DEFAULT NULL,
  `START_RANGE` double DEFAULT NULL,
  `END_RANGE` double DEFAULT NULL,
  `CHARGE_AMOUNT` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DELIVERY_CONFIGURATION`
--

CREATE TABLE `DELIVERY_CONFIGURATION` (
  `ID` int(11) NOT NULL,
  `UNIT_NAME` varchar(20) DEFAULT NULL,
  `UNIT_SYMBOL` varchar(8) DEFAULT NULL,
  `CHARGE_BY_ZIP_CODE` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `DELIVERY_CONFIGURATION`
--

INSERT INTO `DELIVERY_CONFIGURATION` (`ID`, `UNIT_NAME`, `UNIT_SYMBOL`, `CHARGE_BY_ZIP_CODE`) VALUES
(1, 'MILE', NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `DELIVERY_INSTRUCTION`
--

CREATE TABLE `DELIVERY_INSTRUCTION` (
  `ID` int(11) NOT NULL,
  `NOTES` varchar(220) DEFAULT NULL,
  `CUSTOMER_NO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DRAWER_ASSIGNED_HISTORY`
--

CREATE TABLE `DRAWER_ASSIGNED_HISTORY` (
  `ID` int(11) NOT NULL,
  `TIME` datetime DEFAULT NULL,
  `OPERATION` varchar(60) DEFAULT NULL,
  `A_USER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `DRAWER_ASSIGNED_HISTORY`
--

INSERT INTO `DRAWER_ASSIGNED_HISTORY` (`ID`, `TIME`, `OPERATION`, `A_USER`) VALUES
(1, '2018-08-11 14:43:36', 'ASSIGN', 3);

-- --------------------------------------------------------

--
-- Table structure for table `DRAWER_PULL_REPORT`
--

CREATE TABLE `DRAWER_PULL_REPORT` (
  `ID` int(11) NOT NULL,
  `REPORT_TIME` datetime DEFAULT NULL,
  `REG` varchar(15) DEFAULT NULL,
  `TICKET_COUNT` int(11) DEFAULT NULL,
  `BEGIN_CASH` double DEFAULT NULL,
  `NET_SALES` double DEFAULT NULL,
  `SALES_TAX` double DEFAULT NULL,
  `CASH_TAX` double DEFAULT NULL,
  `TOTAL_REVENUE` double DEFAULT NULL,
  `GROSS_RECEIPTS` double DEFAULT NULL,
  `GIFTCERTRETURNCOUNT` int(11) DEFAULT NULL,
  `GIFTCERTRETURNAMOUNT` double DEFAULT NULL,
  `GIFTCERTCHANGEAMOUNT` double DEFAULT NULL,
  `CASH_RECEIPT_NO` int(11) DEFAULT NULL,
  `CASH_RECEIPT_AMOUNT` double DEFAULT NULL,
  `CREDIT_CARD_RECEIPT_NO` int(11) DEFAULT NULL,
  `CREDIT_CARD_RECEIPT_AMOUNT` double DEFAULT NULL,
  `DEBIT_CARD_RECEIPT_NO` int(11) DEFAULT NULL,
  `DEBIT_CARD_RECEIPT_AMOUNT` double DEFAULT NULL,
  `REFUND_RECEIPT_COUNT` int(11) DEFAULT NULL,
  `REFUND_AMOUNT` double DEFAULT NULL,
  `RECEIPT_DIFFERENTIAL` double DEFAULT NULL,
  `CASH_BACK` double DEFAULT NULL,
  `CASH_TIPS` double DEFAULT NULL,
  `CHARGED_TIPS` double DEFAULT NULL,
  `TIPS_PAID` double DEFAULT NULL,
  `TIPS_DIFFERENTIAL` double DEFAULT NULL,
  `PAY_OUT_NO` int(11) DEFAULT NULL,
  `PAY_OUT_AMOUNT` double DEFAULT NULL,
  `DRAWER_BLEED_NO` int(11) DEFAULT NULL,
  `DRAWER_BLEED_AMOUNT` double DEFAULT NULL,
  `DRAWER_ACCOUNTABLE` double DEFAULT NULL,
  `CASH_TO_DEPOSIT` double DEFAULT NULL,
  `VARIANCE` double DEFAULT NULL,
  `DELIVERY_CHARGE` double DEFAULT NULL,
  `totalVoidWst` double DEFAULT NULL,
  `totalVoid` double DEFAULT NULL,
  `totalDiscountCount` int(11) DEFAULT NULL,
  `totalDiscountAmount` double DEFAULT NULL,
  `totalDiscountSales` double DEFAULT NULL,
  `totalDiscountGuest` int(11) DEFAULT NULL,
  `totalDiscountPartySize` int(11) DEFAULT NULL,
  `totalDiscountCheckSize` int(11) DEFAULT NULL,
  `totalDiscountPercentage` double DEFAULT NULL,
  `totalDiscountRatio` double DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DRAWER_PULL_REPORT_VOIDTICKETS`
--

CREATE TABLE `DRAWER_PULL_REPORT_VOIDTICKETS` (
  `DPREPORT_ID` int(11) NOT NULL,
  `code` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `hast` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `EMPLOYEE_IN_OUT_HISTORY`
--

CREATE TABLE `EMPLOYEE_IN_OUT_HISTORY` (
  `ID` int(11) NOT NULL,
  `OUT_TIME` datetime DEFAULT NULL,
  `IN_TIME` datetime DEFAULT NULL,
  `OUT_HOUR` smallint(6) DEFAULT NULL,
  `IN_HOUR` smallint(6) DEFAULT NULL,
  `CLOCK_OUT` bit(1) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `SHIFT_ID` int(11) DEFAULT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GLOBAL_CONFIG`
--

CREATE TABLE `GLOBAL_CONFIG` (
  `ID` int(11) NOT NULL,
  `POS_KEY` varchar(60) DEFAULT NULL,
  `POS_VALUE` varchar(220) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GRATUITY`
--

CREATE TABLE `GRATUITY` (
  `ID` int(11) NOT NULL,
  `AMOUNT` double DEFAULT NULL,
  `PAID` bit(1) DEFAULT NULL,
  `REFUNDED` bit(1) DEFAULT NULL,
  `TICKET_ID` int(11) DEFAULT NULL,
  `OWNER_ID` int(11) DEFAULT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_GROUP`
--

CREATE TABLE `INVENTORY_GROUP` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `VISIBLE` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_ITEM`
--

CREATE TABLE `INVENTORY_ITEM` (
  `ID` int(11) NOT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `LAST_UPDATE_DATE` datetime DEFAULT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `PACKAGE_BARCODE` varchar(30) DEFAULT NULL,
  `UNIT_BARCODE` varchar(30) DEFAULT NULL,
  `UNIT_PER_PACKAGE` double DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `PACKAGE_REORDER_LEVEL` int(11) DEFAULT NULL,
  `PACKAGE_REPLENISH_LEVEL` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `AVERAGE_PACKAGE_PRICE` double DEFAULT NULL,
  `TOTAL_PACKAGES` int(11) DEFAULT NULL,
  `TOTAL_RECEPIE_UNITS` double DEFAULT NULL,
  `UNIT_PURCHASE_PRICE` double DEFAULT NULL,
  `UNIT_SELLING_PRICE` double DEFAULT NULL,
  `VISIBLE` bit(1) DEFAULT NULL,
  `PUNIT_ID` int(11) DEFAULT NULL,
  `RECIPE_UNIT_ID` int(11) DEFAULT NULL,
  `ITEM_GROUP_ID` int(11) DEFAULT NULL,
  `ITEM_LOCATION_ID` int(11) DEFAULT NULL,
  `ITEM_VENDOR_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_LOCATION`
--

CREATE TABLE `INVENTORY_LOCATION` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `VISIBLE` bit(1) DEFAULT NULL,
  `WAREHOUSE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_META_CODE`
--

CREATE TABLE `INVENTORY_META_CODE` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `CODE_TEXT` varchar(255) DEFAULT NULL,
  `CODE_NO` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_TRANSACTION`
--

CREATE TABLE `INVENTORY_TRANSACTION` (
  `ID` int(11) NOT NULL,
  `TRANSACTION_DATE` datetime DEFAULT NULL,
  `QUANTITY` int(11) DEFAULT NULL,
  `UNIT_PRICE` double DEFAULT NULL,
  `REMARK` varchar(255) DEFAULT NULL,
  `TRAN_TYPE` int(11) DEFAULT NULL,
  `REFERENCE_ID` int(11) DEFAULT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `VENDOR_ID` int(11) DEFAULT NULL,
  `FROM_WAREHOUSE_ID` int(11) DEFAULT NULL,
  `TO_WAREHOUSE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_UNIT`
--

CREATE TABLE `INVENTORY_UNIT` (
  `ID` int(11) NOT NULL,
  `SHORT_NAME` varchar(255) DEFAULT NULL,
  `LONG_NAME` varchar(255) DEFAULT NULL,
  `ALT_NAME` varchar(255) DEFAULT NULL,
  `CONV_FACTOR1` varchar(255) DEFAULT NULL,
  `CONV_FACTOR2` varchar(255) DEFAULT NULL,
  `CONV_FACTOR3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_VENDOR`
--

CREATE TABLE `INVENTORY_VENDOR` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `VISIBLE` bit(1) DEFAULT NULL,
  `ADDRESS` varchar(120) NOT NULL,
  `CITY` varchar(60) NOT NULL,
  `STATE` varchar(60) NOT NULL,
  `ZIP` varchar(60) NOT NULL,
  `COUNTRY` varchar(60) NOT NULL,
  `EMAIL` varchar(60) NOT NULL,
  `PHONE` varchar(60) NOT NULL,
  `FAX` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `INVENTORY_WAREHOUSE`
--

CREATE TABLE `INVENTORY_WAREHOUSE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `VISIBLE` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ITEM_ORDER_TYPE`
--

CREATE TABLE `ITEM_ORDER_TYPE` (
  `MENU_ITEM_ID` int(11) NOT NULL,
  `ORDER_TYPE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `KITCHEN_TICKET`
--

CREATE TABLE `KITCHEN_TICKET` (
  `ID` int(11) NOT NULL,
  `TICKET_ID` int(11) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `CLOSE_DATE` datetime DEFAULT NULL,
  `VOIDED` bit(1) DEFAULT NULL,
  `SEQUENCE_NUMBER` int(11) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `SERVER_NAME` varchar(30) DEFAULT NULL,
  `TICKET_TYPE` varchar(20) DEFAULT NULL,
  `PG_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `KITCHEN_TICKET_ITEM`
--

CREATE TABLE `KITCHEN_TICKET_ITEM` (
  `ID` int(11) NOT NULL,
  `COOKABLE` bit(1) DEFAULT NULL,
  `TICKET_ITEM_ID` int(11) DEFAULT NULL,
  `TICKET_ITEM_MODIFIER_ID` int(11) DEFAULT NULL,
  `MENU_ITEM_CODE` varchar(255) DEFAULT NULL,
  `MENU_ITEM_NAME` varchar(120) DEFAULT NULL,
  `MENU_ITEM_GROUP_ID` int(11) DEFAULT NULL,
  `MENU_ITEM_GROUP_NAME` varchar(120) DEFAULT NULL,
  `QUANTITY` int(11) DEFAULT NULL,
  `FRACTIONAL_QUANTITY` double DEFAULT NULL,
  `FRACTIONAL_UNIT` bit(1) DEFAULT NULL,
  `UNIT_NAME` varchar(20) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `VOIDED` bit(1) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `KITHEN_TICKET_ID` int(11) DEFAULT NULL,
  `ITEM_ORDER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `KIT_TICKET_TABLE_NUM`
--

CREATE TABLE `KIT_TICKET_TABLE_NUM` (
  `kit_ticket_id` int(11) NOT NULL,
  `TABLE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENUCATEGORY_DISCOUNT`
--

CREATE TABLE `MENUCATEGORY_DISCOUNT` (
  `DISCOUNT_ID` int(11) NOT NULL,
  `MENUCATEGORY_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENUGROUP_DISCOUNT`
--

CREATE TABLE `MENUGROUP_DISCOUNT` (
  `DISCOUNT_ID` int(11) NOT NULL,
  `MENUGROUP_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENUITEM_DISCOUNT`
--

CREATE TABLE `MENUITEM_DISCOUNT` (
  `DISCOUNT_ID` int(11) NOT NULL,
  `MENUITEM_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENUITEM_MODIFIERGROUP`
--

CREATE TABLE `MENUITEM_MODIFIERGROUP` (
  `ID` int(11) NOT NULL,
  `MIN_QUANTITY` int(11) DEFAULT NULL,
  `MAX_QUANTITY` int(11) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `MODIFIER_GROUP` int(11) DEFAULT NULL,
  `MENUITEM_MODIFIERGROUP_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENUITEM_MODIFIERGROUP`
--

INSERT INTO `MENUITEM_MODIFIERGROUP` (`ID`, `MIN_QUANTITY`, `MAX_QUANTITY`, `SORT_ORDER`, `MODIFIER_GROUP`, `MENUITEM_MODIFIERGROUP_ID`) VALUES
(1, 1, 1, 0, 6, 1),
(2, 1, 2, 0, 2, 1),
(3, 1, 2, 0, 6, 2),
(4, 1, 0, 0, 3, 2),
(5, 0, 1, 0, 4, 2),
(6, 0, 1, 0, 1, 26),
(7, 0, 2, 0, 2, 26),
(8, 2, 2, 0, 6, 3),
(9, 0, 1, 0, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `MENUITEM_PIZZAPIRCE`
--

CREATE TABLE `MENUITEM_PIZZAPIRCE` (
  `MENU_ITEM_ID` int(11) NOT NULL,
  `PIZZA_PRICE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENUITEM_SHIFT`
--

CREATE TABLE `MENUITEM_SHIFT` (
  `ID` int(11) NOT NULL,
  `SHIFT_PRICE` double DEFAULT NULL,
  `SHIFT_ID` int(11) DEFAULT NULL,
  `MENUITEM_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENUMODIFIER_PIZZAMODIFIERPRICE`
--

CREATE TABLE `MENUMODIFIER_PIZZAMODIFIERPRICE` (
  `MENUMODIFIER_ID` int(11) NOT NULL,
  `PIZZAMODIFIERPRICE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENU_CATEGORY`
--

CREATE TABLE `MENU_CATEGORY` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(120) NOT NULL,
  `TRANSLATED_NAME` varchar(120) DEFAULT NULL,
  `VISIBLE` bit(1) DEFAULT NULL,
  `BEVERAGE` bit(1) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `BTN_COLOR` int(11) DEFAULT NULL,
  `TEXT_COLOR` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENU_CATEGORY`
--

INSERT INTO `MENU_CATEGORY` (`ID`, `NAME`, `TRANSLATED_NAME`, `VISIBLE`, `BEVERAGE`, `SORT_ORDER`, `BTN_COLOR`, `TEXT_COLOR`) VALUES
(1, 'APPETIZERS', '', b'1', b'0', 9999, -154, -16777216),
(2, 'BEER & WINE', '', b'0', b'1', 9999, -26215, -16777216),
(3, 'BEVERAGE & DRINKS', '', b'1', b'1', 9999, -16737895, -16777216),
(4, 'BREAKFAST', '', b'1', b'0', 9999, -16724890, -16777216),
(5, 'BUFFET', '', b'0', b'0', 9999, -6711040, -16777216),
(6, 'DESSERT & ICECRM', '', b'1', b'0', 9999, -10066177, -16777216),
(7, 'FAVORITES', '', b'1', b'0', 9999, -65281, -16777216),
(8, 'KIDS ', '', b'1', b'0', 9999, -16711885, -16777216),
(9, 'LUNCH N DINNER', '', b'1', b'0', 9999, -6750055, -16777216),
(10, 'SIDES', '', b'1', b'0', 9999, -3355444, -16777216);

-- --------------------------------------------------------

--
-- Table structure for table `MENU_GROUP`
--

CREATE TABLE `MENU_GROUP` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(120) NOT NULL,
  `TRANSLATED_NAME` varchar(120) DEFAULT NULL,
  `VISIBLE` bit(1) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `BTN_COLOR` int(11) DEFAULT NULL,
  `TEXT_COLOR` int(11) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENU_GROUP`
--

INSERT INTO `MENU_GROUP` (`ID`, `NAME`, `TRANSLATED_NAME`, `VISIBLE`, `SORT_ORDER`, `BTN_COLOR`, `TEXT_COLOR`, `CATEGORY_ID`) VALUES
(1, 'BEERS', NULL, b'1', 9999, 0, 0, 2),
(2, 'CEREALS & MUFF', NULL, b'1', 9999, 0, 0, 4),
(3, 'COLD BEVERAGE', NULL, b'1', 9999, 0, 0, 3),
(4, 'FAVOURITE', NULL, b'1', 9999, 0, 0, 4),
(5, 'FRENCH FRIES', NULL, b'1', 9999, 0, 0, 1),
(6, 'HOT DRINKS', NULL, b'1', 9999, 0, 0, 3),
(7, 'JUICES', NULL, b'1', 9999, 0, 0, 3),
(8, 'KIDS MEAL', NULL, b'1', 9999, 0, 0, 8),
(9, 'ONION RINGS', NULL, b'0', 9999, 0, 0, 1),
(10, 'PANCAKE N SUCH', NULL, b'1', 9999, 0, 0, 4),
(11, 'REDS', NULL, b'1', 9999, 0, 0, 2),
(12, 'SANDWITCH PLATTER', NULL, b'1', 9999, 0, 0, 9),
(13, 'SIDES', NULL, b'1', 9999, 0, 0, 10),
(14, 'SILE PLATES', NULL, b'1', 9999, 0, 0, 4),
(15, 'TRADITIONAL B.FAST', NULL, b'1', 9999, 0, 0, 4),
(16, 'WHITES', NULL, b'1', 9999, 0, 0, 2),
(17, 'WHOLESOME', NULL, b'1', 9999, 0, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `MENU_ITEM`
--

CREATE TABLE `MENU_ITEM` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(120) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UNIT_NAME` varchar(20) DEFAULT NULL,
  `TRANSLATED_NAME` varchar(120) DEFAULT NULL,
  `BARCODE` varchar(120) DEFAULT NULL,
  `BUY_PRICE` double NOT NULL,
  `STOCK_AMOUNT` double DEFAULT NULL,
  `PRICE` double NOT NULL,
  `DISCOUNT_RATE` double DEFAULT NULL,
  `VISIBLE` bit(1) DEFAULT NULL,
  `DISABLE_WHEN_STOCK_AMOUNT_IS_ZERO` bit(1) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `BTN_COLOR` int(11) DEFAULT NULL,
  `TEXT_COLOR` int(11) DEFAULT NULL,
  `IMAGE` blob,
  `SHOW_IMAGE_ONLY` bit(1) DEFAULT NULL,
  `FRACTIONAL_UNIT` bit(1) DEFAULT NULL,
  `PIZZA_TYPE` bit(1) DEFAULT NULL,
  `DEFAULT_SELL_PORTION` int(11) DEFAULT NULL,
  `GROUP_ID` int(11) DEFAULT NULL,
  `TAX_ID` int(11) DEFAULT NULL,
  `RECEPIE` int(11) DEFAULT NULL,
  `PG_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENU_ITEM`
--

INSERT INTO `MENU_ITEM` (`ID`, `NAME`, `DESCRIPTION`, `UNIT_NAME`, `TRANSLATED_NAME`, `BARCODE`, `BUY_PRICE`, `STOCK_AMOUNT`, `PRICE`, `DISCOUNT_RATE`, `VISIBLE`, `DISABLE_WHEN_STOCK_AMOUNT_IS_ZERO`, `SORT_ORDER`, `BTN_COLOR`, `TEXT_COLOR`, `IMAGE`, `SHOW_IMAGE_ONLY`, `FRACTIONAL_UNIT`, `PIZZA_TYPE`, `DEFAULT_SELL_PORTION`, `GROUP_ID`, `TAX_ID`, `RECEPIE`, `PG_ID`) VALUES
(1, '1 EGG BREAKFAST', NULL, NULL, '1 EGG BREAKFAST', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, 1, NULL, NULL),
(2, '2 EGG N BISCUIT', NULL, NULL, '2 EGG N BISCUIT', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, NULL, NULL, NULL),
(3, '2 EGG SANDWITCH', NULL, NULL, '2 EGG SANDWITCH', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, NULL, NULL, NULL),
(4, '4 VINES CALIF', NULL, NULL, '4 VINES CALIF', NULL, 0, 0, 0, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 11, NULL, NULL, NULL),
(5, 'A 2 Z CHARDONNAY', NULL, NULL, 'A 2 Z CHARDONNAY', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 16, 1, NULL, NULL),
(6, 'APPLE DIPPER', NULL, NULL, 'APPLE DIPPER', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 13, 1, NULL, NULL),
(7, 'APPLE JUICE', NULL, NULL, 'APPLE JUICE', NULL, 0, 0, 0.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 7, 1, NULL, NULL),
(8, 'ASSORT CEREAL', NULL, NULL, 'ASSORT CEREAL', NULL, 0, 0, 1.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 2, 1, NULL, NULL),
(9, 'AVALON NAPA', NULL, NULL, 'AVALON NAPA', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 11, 1, NULL, NULL),
(10, 'BELLYS SEASONAL', NULL, NULL, 'BELLYS SEASONAL', NULL, 0, 0, 4, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 1, 1, NULL, NULL),
(11, 'CEASAR SALAD', NULL, NULL, 'CEASAR SALAD', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 13, 1, NULL, NULL),
(12, 'CHICK BLT', NULL, NULL, 'CHICK BLT', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 12, 1, NULL, NULL),
(13, 'CHICK TENDER KIDS MEAL', NULL, NULL, 'CHICK TENDER KIDS MEAL', NULL, 0, 0, 4, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 8, 1, NULL, NULL),
(14, 'CHOC MILK', NULL, NULL, 'CHOC MILK', NULL, 0, 0, 2.25, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 3, 1, NULL, NULL),
(15, 'CUPPOCINO', NULL, NULL, 'CUPPOCINO', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 6, 1, NULL, NULL),
(16, 'DBL MEAT BRK', NULL, NULL, 'DBL MEAT BRK', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 15, 1, NULL, NULL),
(17, 'DISENO MALBEC', NULL, NULL, 'DISENO MALBEC', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 11, 1, NULL, NULL),
(18, 'FELLUGO ITALY', NULL, NULL, 'FELLUGO ITALY', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 11, 1, NULL, NULL),
(19, 'FIRESTEED OREGON', NULL, NULL, 'FIRESTEED OREGON', NULL, 0, 0, 4, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 16, 1, NULL, NULL),
(20, 'GO GO SQUEEZE', NULL, NULL, 'GO GO SQUEEZE', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 13, 1, NULL, NULL),
(21, 'GRANDPA CNTRY B.FAST', NULL, NULL, 'GRANDPA CNTRY B.FAST', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 15, 1, NULL, NULL),
(22, 'GRAVY N BISCUIT', NULL, NULL, 'GRAVY N BISCUIT', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, 1, NULL, NULL),
(23, 'GRILLED CHICKEN', NULL, NULL, 'GRILLED CHICKEN', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 12, 1, NULL, NULL),
(24, 'HAMBURGER HALFPOUND', NULL, NULL, 'HAMBURGER HALFPOUND', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 12, 1, NULL, NULL),
(25, 'HAMMER COFFEE', NULL, NULL, 'HAMMER COFFEE', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 6, 1, NULL, NULL),
(26, 'HASHBROWN COMBO', NULL, NULL, 'HASHBROWN COMBO', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, 1, NULL, NULL),
(27, 'ICE TEA', NULL, NULL, 'ICE TEA', NULL, 0, 0, 0, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 3, 1, NULL, NULL),
(28, 'KENWOOD YULUPA', NULL, NULL, 'KENWOOD YULUPA', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 16, 1, NULL, NULL),
(29, 'LEMONADE', NULL, NULL, 'LEMONADE', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 3, 1, NULL, NULL),
(30, 'MEAT BISCUIT N HASHBROWN', NULL, NULL, 'MEAT BISCUIT N HASHBROWN', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, 1, NULL, NULL),
(31, 'MICHELOB GOLDEN', NULL, NULL, 'MICHELOB GOLDEN', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 1, 1, NULL, NULL),
(32, 'MNT DEW', NULL, NULL, 'MNT DEW', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 3, 1, NULL, NULL),
(33, 'MOZZARELLA STICK', NULL, NULL, 'MOZZARELLA STICK', NULL, 0, 0, 0, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 13, 1, NULL, NULL),
(34, 'OATMEAL ', NULL, NULL, 'OATMEAL ', NULL, 0, 0, 1.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 2, 1, NULL, NULL),
(35, 'OLD TIMER B.FAST', NULL, NULL, 'OLD TIMER B.FAST', NULL, 0, 0, 2.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 15, 1, NULL, NULL),
(36, 'OVN ROAST TURKEY', NULL, NULL, 'OVN ROAST TURKEY', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 12, 1, NULL, NULL),
(37, 'SCHELLS FIREBRICH AMBER', NULL, NULL, 'SCHELLS FIREBRICH AMBER', NULL, 0, 0, 4, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 1, 1, NULL, NULL),
(38, 'SMK HOUS B.FAST', NULL, NULL, 'SMK HOUS B.FAST', NULL, 0, 0, 2.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 15, 1, NULL, NULL),
(39, 'SODA', NULL, NULL, 'SODA', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 3, 1, NULL, NULL),
(40, 'SOURDOUGH TOAST', NULL, NULL, 'SOURDOUGH TOAST', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 4, 1, NULL, NULL),
(41, 'STEELHEAD', NULL, NULL, 'STEELHEAD', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 11, 1, NULL, NULL),
(42, 'STELLA ARTOIS ', NULL, NULL, 'STELLA ARTOIS ', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 1, 1, NULL, NULL),
(43, 'SUMMIT PALE ALE', NULL, NULL, 'SUMMIT PALE ALE', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 1, 1, NULL, NULL),
(44, 'SUNRISE SAMPLER', NULL, NULL, 'SUNRISE SAMPLER', NULL, 0, 0, 3.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 15, 1, NULL, NULL),
(45, 'SURLY FURIOUS', NULL, NULL, 'SURLY FURIOUS', NULL, 0, 0, 4, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 1, 1, NULL, NULL),
(46, 'TANGLE OAKS', NULL, NULL, 'TANGLE OAKS', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 16, 1, NULL, NULL),
(47, 'TRIADE ITALIAN', NULL, NULL, 'TRIADE ITALIAN', NULL, 0, 0, 3, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 16, 1, NULL, NULL),
(48, 'VANILLA YOUGURT', NULL, NULL, 'VANILLA YOUGURT', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 13, 1, NULL, NULL),
(49, 'VIENNA DECAF', NULL, NULL, 'VIENNA DECAF', NULL, 0, 0, 1, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 6, 1, NULL, NULL),
(50, 'VINA MAYOR', NULL, NULL, 'VINA MAYOR', NULL, 0, 0, 2, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 11, 1, NULL, NULL),
(51, 'WAIRAU RIVER', NULL, NULL, 'WAIRAU RIVER', NULL, 0, 0, 4, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 16, 1, NULL, NULL),
(52, 'YOGURT PRF', NULL, NULL, 'YOGURT PRF', NULL, 0, 0, 2.5, 0, b'1', b'0', 9999, NULL, NULL, NULL, b'0', b'0', b'0', 0, 2, 1, NULL, NULL),
(53, 'Pure', '', '', 'Githeri', '', 0, 0, 0, 0, b'1', b'0', 9999, -1250856, -16777216, NULL, b'0', b'0', b'0', 0, 4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MENU_ITEM_PROPERTIES`
--

CREATE TABLE `MENU_ITEM_PROPERTIES` (
  `MENU_ITEM_ID` int(11) NOT NULL,
  `PROPERTY_VALUE` varchar(100) DEFAULT NULL,
  `PROPERTY_NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENU_ITEM_SIZE`
--

CREATE TABLE `MENU_ITEM_SIZE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `TRANSLATED_NAME` varchar(60) DEFAULT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `SIZE_IN_INCH` double DEFAULT NULL,
  `DEFAULT_SIZE` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENU_ITEM_SIZE`
--

INSERT INTO `MENU_ITEM_SIZE` (`ID`, `NAME`, `TRANSLATED_NAME`, `DESCRIPTION`, `SORT_ORDER`, `SIZE_IN_INCH`, `DEFAULT_SIZE`) VALUES
(1, 'SMALL', 'SMALL', NULL, 0, 0, b'0'),
(2, 'MEDIUM', 'MEDIUM', NULL, 1, 0, b'0'),
(3, 'LARGE', 'LARGE', NULL, 2, 0, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `MENU_ITEM_TERMINAL_REF`
--

CREATE TABLE `MENU_ITEM_TERMINAL_REF` (
  `MENU_ITEM_ID` int(11) NOT NULL,
  `TERMINAL_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MENU_MODIFIER`
--

CREATE TABLE `MENU_MODIFIER` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(120) DEFAULT NULL,
  `TRANSLATED_NAME` varchar(120) DEFAULT NULL,
  `PRICE` double DEFAULT NULL,
  `EXTRA_PRICE` double DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `BTN_COLOR` int(11) DEFAULT NULL,
  `TEXT_COLOR` int(11) DEFAULT NULL,
  `ENABLE` bit(1) DEFAULT NULL,
  `FIXED_PRICE` bit(1) DEFAULT NULL,
  `PRINT_TO_KITCHEN` bit(1) DEFAULT NULL,
  `SECTION_WISE_PRICING` bit(1) DEFAULT NULL,
  `PIZZA_MODIFIER` bit(1) DEFAULT NULL,
  `GROUP_ID` int(11) DEFAULT NULL,
  `TAX_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENU_MODIFIER`
--

INSERT INTO `MENU_MODIFIER` (`ID`, `NAME`, `TRANSLATED_NAME`, `PRICE`, `EXTRA_PRICE`, `SORT_ORDER`, `BTN_COLOR`, `TEXT_COLOR`, `ENABLE`, `FIXED_PRICE`, `PRINT_TO_KITCHEN`, `SECTION_WISE_PRICING`, `PIZZA_MODIFIER`, `GROUP_ID`, `TAX_ID`) VALUES
(1, 'AMERICAN', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 5, 1),
(2, 'BACON', NULL, 0.5, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 2, 1),
(3, 'BLEU CHEESE', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 5, 1),
(4, 'Energy Plus', NULL, 0, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 3, 1),
(5, 'FRIED APPLE', NULL, 0, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 2, 1),
(6, 'FRIED EGG', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 6, 1),
(7, 'HASHB CASSEROLE', NULL, 0, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 2, 1),
(8, 'HASHBROWN', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 8, 1),
(9, 'OMLETTE', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 6, 1),
(10, 'PAPER JACK', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 5, 1),
(11, 'PROVOLON', NULL, 0.5, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 5, 1),
(12, 'SAUSAGE', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 2, 1),
(13, 'SCRAMBLED', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 6, 1),
(14, 'SMOKY SAUSAGE', NULL, 0, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 2, 1),
(15, 'SUN SIDE UP', NULL, 0, 0.5, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 6, 1),
(16, 'THICK BACON', NULL, 0, 0, 9999, NULL, NULL, b'0', b'0', b'1', b'0', b'0', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `MENU_MODIFIER_GROUP`
--

CREATE TABLE `MENU_MODIFIER_GROUP` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `TRANSLATED_NAME` varchar(60) DEFAULT NULL,
  `ENABLED` bit(1) DEFAULT NULL,
  `EXCLUSIVED` bit(1) DEFAULT NULL,
  `REQUIRED` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MENU_MODIFIER_GROUP`
--

INSERT INTO `MENU_MODIFIER_GROUP` (`ID`, `NAME`, `TRANSLATED_NAME`, `ENABLED`, `EXCLUSIVED`, `REQUIRED`) VALUES
(1, 'APPLE SLICE', NULL, b'0', b'0', b'0'),
(2, 'BACON & SAUSAGE', NULL, b'0', b'0', b'0'),
(3, 'BISCUITS', NULL, b'0', b'0', b'0'),
(4, 'BUTTER', NULL, b'0', b'0', b'0'),
(5, 'CHEESE', NULL, b'0', b'0', b'0'),
(6, 'EGGS', NULL, b'0', b'0', b'0'),
(7, 'MEAT', NULL, b'0', b'0', b'0'),
(8, 'POTATO', NULL, b'0', b'0', b'0'),
(9, 'WHEAT', NULL, b'0', b'0', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `MENU_MODIFIER_PROPERTIES`
--

CREATE TABLE `MENU_MODIFIER_PROPERTIES` (
  `MENU_MODIFIER_ID` int(11) NOT NULL,
  `PROPERTY_VALUE` varchar(100) DEFAULT NULL,
  `PROPERTY_NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MODIFIER_MULTIPLIER_PRICE`
--

CREATE TABLE `MODIFIER_MULTIPLIER_PRICE` (
  `ID` int(11) NOT NULL,
  `PRICE` double DEFAULT NULL,
  `MULTIPLIER_ID` varchar(20) DEFAULT NULL,
  `MENUMODIFIER_ID` int(11) DEFAULT NULL,
  `PIZZA_MODIFIER_PRICE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MULTIPLIER`
--

CREATE TABLE `MULTIPLIER` (
  `NAME` varchar(20) NOT NULL,
  `TICKET_PREFIX` varchar(20) DEFAULT NULL,
  `RATE` double DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `DEFAULT_MULTIPLIER` bit(1) DEFAULT NULL,
  `MAIN` bit(1) DEFAULT NULL,
  `BTN_COLOR` int(11) DEFAULT NULL,
  `TEXT_COLOR` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MULTIPLIER`
--

INSERT INTO `MULTIPLIER` (`NAME`, `TICKET_PREFIX`, `RATE`, `SORT_ORDER`, `DEFAULT_MULTIPLIER`, `MAIN`, `BTN_COLOR`, `TEXT_COLOR`) VALUES
('Extra', 'Extra', 200, 4, b'0', b'0', NULL, NULL),
('Half', 'Half', 50, 2, b'0', b'0', NULL, NULL),
('No', 'No', 0, 1, b'0', b'0', NULL, NULL),
('Quarter', 'Quarter', 25, 3, b'0', b'0', NULL, NULL),
('Regular', '', 0, 0, b'1', b'1', NULL, NULL),
('Sub', 'Sub', 100, 6, b'0', b'0', NULL, NULL),
('Triple', 'Triple', 300, 5, b'0', b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ORDER_TYPE`
--

CREATE TABLE `ORDER_TYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(120) NOT NULL,
  `ENABLED` bit(1) DEFAULT NULL,
  `SHOW_TABLE_SELECTION` bit(1) DEFAULT NULL,
  `SHOW_GUEST_SELECTION` bit(1) DEFAULT NULL,
  `SHOULD_PRINT_TO_KITCHEN` bit(1) DEFAULT NULL,
  `PREPAID` bit(1) DEFAULT NULL,
  `CLOSE_ON_PAID` bit(1) DEFAULT NULL,
  `REQUIRED_CUSTOMER_DATA` bit(1) DEFAULT NULL,
  `DELIVERY` bit(1) DEFAULT NULL,
  `SHOW_ITEM_BARCODE` bit(1) DEFAULT NULL,
  `SHOW_IN_LOGIN_SCREEN` bit(1) DEFAULT NULL,
  `CONSOLIDATE_TIEMS_IN_RECEIPT` bit(1) DEFAULT NULL,
  `ALLOW_SEAT_BASED_ORDER` bit(1) DEFAULT NULL,
  `HIDE_ITEM_WITH_EMPTY_INVENTORY` bit(1) DEFAULT NULL,
  `HAS_FORHERE_AND_TOGO` bit(1) DEFAULT NULL,
  `PRE_AUTH_CREDIT_CARD` bit(1) DEFAULT NULL,
  `BAR_TAB` bit(1) DEFAULT NULL,
  `SHOW_PRICE_ON_BUTTON` bit(1) DEFAULT NULL,
  `SHOW_STOCK_COUNT_ON_BUTTON` bit(1) DEFAULT NULL,
  `SHOW_UNIT_PRICE_IN_TICKET_GRID` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ORDER_TYPE`
--

INSERT INTO `ORDER_TYPE` (`ID`, `NAME`, `ENABLED`, `SHOW_TABLE_SELECTION`, `SHOW_GUEST_SELECTION`, `SHOULD_PRINT_TO_KITCHEN`, `PREPAID`, `CLOSE_ON_PAID`, `REQUIRED_CUSTOMER_DATA`, `DELIVERY`, `SHOW_ITEM_BARCODE`, `SHOW_IN_LOGIN_SCREEN`, `CONSOLIDATE_TIEMS_IN_RECEIPT`, `ALLOW_SEAT_BASED_ORDER`, `HIDE_ITEM_WITH_EMPTY_INVENTORY`, `HAS_FORHERE_AND_TOGO`, `PRE_AUTH_CREDIT_CARD`, `BAR_TAB`, `SHOW_PRICE_ON_BUTTON`, `SHOW_STOCK_COUNT_ON_BUTTON`, `SHOW_UNIT_PRICE_IN_TICKET_GRID`) VALUES
(1, 'DINE IN', b'1', b'1', b'0', b'1', b'0', b'1', b'0', b'0', b'0', b'1', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0'),
(2, 'TAKE OUT', b'1', b'0', b'0', b'1', b'1', b'1', b'0', b'0', b'0', b'1', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0'),
(3, 'RETAIL', b'1', b'0', b'0', b'0', b'0', b'1', b'0', b'0', b'0', b'1', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0'),
(4, 'HOME DELIVERY', b'1', b'0', b'0', b'1', b'0', b'0', b'1', b'1', b'0', b'1', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `PACKAGING_UNIT`
--

CREATE TABLE `PACKAGING_UNIT` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(30) DEFAULT NULL,
  `SHORT_NAME` varchar(10) DEFAULT NULL,
  `FACTOR` double DEFAULT NULL,
  `BASEUNIT` bit(1) DEFAULT NULL,
  `DIMENSION` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PAYOUT_REASONS`
--

CREATE TABLE `PAYOUT_REASONS` (
  `ID` int(11) NOT NULL,
  `REASON` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PAYOUT_RECEPIENTS`
--

CREATE TABLE `PAYOUT_RECEPIENTS` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PIZZA_CRUST`
--

CREATE TABLE `PIZZA_CRUST` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `TRANSLATED_NAME` varchar(60) DEFAULT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `SORT_ORDER` int(11) DEFAULT NULL,
  `DEFAULT_CRUST` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PIZZA_CRUST`
--

INSERT INTO `PIZZA_CRUST` (`ID`, `NAME`, `TRANSLATED_NAME`, `DESCRIPTION`, `SORT_ORDER`, `DEFAULT_CRUST`) VALUES
(1, 'PAN', 'PAN', NULL, 0, b'0'),
(2, 'HAND TOSSED', 'HAND TOSSED', NULL, 1, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `PIZZA_MODIFIER_PRICE`
--

CREATE TABLE `PIZZA_MODIFIER_PRICE` (
  `ID` int(11) NOT NULL,
  `ITEM_SIZE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PIZZA_PRICE`
--

CREATE TABLE `PIZZA_PRICE` (
  `ID` int(11) NOT NULL,
  `PRICE` double DEFAULT NULL,
  `MENU_ITEM_SIZE` int(11) DEFAULT NULL,
  `CRUST` int(11) DEFAULT NULL,
  `ORDER_TYPE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PRINTER_CONFIGURATION`
--

CREATE TABLE `PRINTER_CONFIGURATION` (
  `ID` int(11) NOT NULL,
  `RECEIPT_PRINTER` varchar(255) DEFAULT NULL,
  `KITCHEN_PRINTER` varchar(255) DEFAULT NULL,
  `PRWTS` bit(1) DEFAULT NULL,
  `PRWTP` bit(1) DEFAULT NULL,
  `PKWTS` bit(1) DEFAULT NULL,
  `PKWTP` bit(1) DEFAULT NULL,
  `UNPFT` bit(1) DEFAULT NULL,
  `UNPFK` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PRINTER_GROUP`
--

CREATE TABLE `PRINTER_GROUP` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `IS_DEFAULT` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PRINTER_GROUP_PRINTERS`
--

CREATE TABLE `PRINTER_GROUP_PRINTERS` (
  `printer_id` int(11) NOT NULL,
  `PRINTER_NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PURCHASE_ORDER`
--

CREATE TABLE `PURCHASE_ORDER` (
  `ID` int(11) NOT NULL,
  `ORDER_ID` varchar(30) DEFAULT NULL,
  `NAME` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `RECEPIE`
--

CREATE TABLE `RECEPIE` (
  `ID` int(11) NOT NULL,
  `MENU_ITEM` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `RECEPIE_ITEM`
--

CREATE TABLE `RECEPIE_ITEM` (
  `ID` int(11) NOT NULL,
  `PERCENTAGE` double DEFAULT NULL,
  `INVENTORY_DEDUCTABLE` bit(1) DEFAULT NULL,
  `INVENTORY_ITEM` int(11) DEFAULT NULL,
  `RECEPIE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `RESTAURANT`
--

CREATE TABLE `RESTAURANT` (
  `ID` int(11) NOT NULL,
  `UNIQUE_ID` int(11) DEFAULT NULL,
  `NAME` varchar(120) DEFAULT NULL,
  `ADDRESS_LINE1` varchar(60) DEFAULT NULL,
  `ADDRESS_LINE2` varchar(60) DEFAULT NULL,
  `ADDRESS_LINE3` varchar(60) DEFAULT NULL,
  `ZIP_CODE` varchar(10) DEFAULT NULL,
  `TELEPHONE` varchar(16) DEFAULT NULL,
  `CAPACITY` int(11) DEFAULT NULL,
  `TABLES` int(11) DEFAULT NULL,
  `CNAME` varchar(20) DEFAULT NULL,
  `CSYMBOL` varchar(10) DEFAULT NULL,
  `SC_PERCENTAGE` double DEFAULT NULL,
  `GRATUITY_PERCENTAGE` double DEFAULT NULL,
  `TICKET_FOOTER` varchar(60) DEFAULT NULL,
  `PRICE_INCLUDES_TAX` bit(1) DEFAULT NULL,
  `ALLOW_MODIFIER_MAX_EXCEED` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `RESTAURANT`
--

INSERT INTO `RESTAURANT` (`ID`, `UNIQUE_ID`, `NAME`, `ADDRESS_LINE1`, `ADDRESS_LINE2`, `ADDRESS_LINE3`, `ZIP_CODE`, `TELEPHONE`, `CAPACITY`, `TABLES`, `CNAME`, `CSYMBOL`, `SC_PERCENTAGE`, `GRATUITY_PERCENTAGE`, `TICKET_FOOTER`, `PRICE_INCLUDES_TAX`, `ALLOW_MODIFIER_MAX_EXCEED`) VALUES
(1, 1422935779, 'Sample Restaurant', 'Somewhere', NULL, NULL, NULL, '+0123456789', 0, 0, 'Sample Currency', '$', 0, 0, NULL, b'0', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `SHIFT`
--

CREATE TABLE `SHIFT` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `START_TIME` datetime DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `SHIFT_LEN` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SHIFT`
--

INSERT INTO `SHIFT` (`ID`, `NAME`, `START_TIME`, `END_TIME`, `SHIFT_LEN`) VALUES
(1, 'General', '1970-01-01 00:00:00', '1970-01-01 23:59:00', 86340000);

-- --------------------------------------------------------

--
-- Table structure for table `SHOP_FLOOR`
--

CREATE TABLE `SHOP_FLOOR` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `OCCUPIED` bit(1) DEFAULT NULL,
  `IMAGE` mediumblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SHOP_FLOOR_TEMPLATE`
--

CREATE TABLE `SHOP_FLOOR_TEMPLATE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `DEFAULT_FLOOR` bit(1) DEFAULT NULL,
  `MAIN` bit(1) DEFAULT NULL,
  `FLOOR_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SHOP_FLOOR_TEMPLATE_PROPERTIES`
--

CREATE TABLE `SHOP_FLOOR_TEMPLATE_PROPERTIES` (
  `id` int(11) NOT NULL,
  `property_value` varchar(60) DEFAULT NULL,
  `property_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SHOP_TABLE`
--

CREATE TABLE `SHOP_TABLE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(60) DEFAULT NULL,
  `CAPACITY` int(11) DEFAULT NULL,
  `X` int(11) DEFAULT NULL,
  `Y` int(11) DEFAULT NULL,
  `FREE` bit(1) DEFAULT NULL,
  `SERVING` bit(1) DEFAULT NULL,
  `BOOKED` bit(1) DEFAULT NULL,
  `DIRTY` bit(1) DEFAULT NULL,
  `DISABLE` bit(1) DEFAULT NULL,
  `FLOOR_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SHOP_TABLE`
--

INSERT INTO `SHOP_TABLE` (`ID`, `NAME`, `DESCRIPTION`, `CAPACITY`, `X`, `Y`, `FREE`, `SERVING`, `BOOKED`, `DIRTY`, `DISABLE`, `FLOOR_ID`) VALUES
(1, NULL, NULL, 4, 0, 0, b'0', b'1', b'0', b'0', b'0', NULL),
(2, NULL, NULL, 4, 0, 0, b'0', b'0', b'0', b'0', b'0', NULL),
(3, NULL, NULL, 4, 0, 0, b'0', b'0', b'0', b'0', b'0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `SHOP_TABLE_TYPE`
--

CREATE TABLE `SHOP_TABLE_TYPE` (
  `ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `NAME` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TABLE_BOOKING_INFO`
--

CREATE TABLE `TABLE_BOOKING_INFO` (
  `ID` int(11) NOT NULL,
  `FROM_DATE` datetime DEFAULT NULL,
  `TO_DATE` datetime DEFAULT NULL,
  `GUEST_COUNT` int(11) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `PAYMENT_STATUS` varchar(30) DEFAULT NULL,
  `BOOKING_CONFIRM` varchar(30) DEFAULT NULL,
  `BOOKING_CHARGE` double DEFAULT NULL,
  `REMAINING_BALANCE` double DEFAULT NULL,
  `PAID_AMOUNT` double DEFAULT NULL,
  `BOOKING_ID` varchar(30) DEFAULT NULL,
  `BOOKING_TYPE` varchar(30) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TABLE_BOOKING_MAPPING`
--

CREATE TABLE `TABLE_BOOKING_MAPPING` (
  `BOOKING_ID` int(11) NOT NULL,
  `TABLE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TABLE_TYPE_RELATION`
--

CREATE TABLE `TABLE_TYPE_RELATION` (
  `TABLE_ID` int(11) NOT NULL,
  `TYPE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TAX`
--

CREATE TABLE `TAX` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) NOT NULL,
  `RATE` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TAX`
--

INSERT INTO `TAX` (`ID`, `NAME`, `RATE`) VALUES
(1, 'US', 6);

-- --------------------------------------------------------

--
-- Table structure for table `TERMINAL`
--

CREATE TABLE `TERMINAL` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) DEFAULT NULL,
  `TERMINAL_KEY` varchar(120) DEFAULT NULL,
  `OPENING_BALANCE` double DEFAULT NULL,
  `CURRENT_BALANCE` double DEFAULT NULL,
  `HAS_CASH_DRAWER` bit(1) DEFAULT NULL,
  `IN_USE` bit(1) DEFAULT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `LOCATION` text,
  `FLOOR_ID` int(11) DEFAULT NULL,
  `ASSIGNED_USER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TERMINAL`
--

INSERT INTO `TERMINAL` (`ID`, `NAME`, `TERMINAL_KEY`, `OPENING_BALANCE`, `CURRENT_BALANCE`, `HAS_CASH_DRAWER`, `IN_USE`, `ACTIVE`, `LOCATION`, `FLOOR_ID`, `ASSIGNED_USER`) VALUES
(7130, '7130', 'e167c85b-d31a-44ce-989b-c9841c5ea2c3', 0, 500, b'1', b'0', b'0', NULL, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `TERMINAL_PRINTERS`
--

CREATE TABLE `TERMINAL_PRINTERS` (
  `ID` int(11) NOT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL,
  `PRINTER_NAME` varchar(60) DEFAULT NULL,
  `VIRTUAL_PRINTER_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET`
--

CREATE TABLE `TICKET` (
  `ID` int(11) NOT NULL,
  `GLOBAL_ID` varchar(16) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `CLOSING_DATE` datetime DEFAULT NULL,
  `ACTIVE_DATE` datetime DEFAULT NULL,
  `DELIVEERY_DATE` datetime DEFAULT NULL,
  `CREATION_HOUR` int(11) DEFAULT NULL,
  `PAID` bit(1) DEFAULT NULL,
  `VOIDED` bit(1) DEFAULT NULL,
  `VOID_REASON` varchar(255) DEFAULT NULL,
  `WASTED` bit(1) DEFAULT NULL,
  `REFUNDED` bit(1) DEFAULT NULL,
  `SETTLED` bit(1) DEFAULT NULL,
  `DRAWER_RESETTED` bit(1) DEFAULT NULL,
  `SUB_TOTAL` double DEFAULT NULL,
  `TOTAL_DISCOUNT` double DEFAULT NULL,
  `TOTAL_TAX` double DEFAULT NULL,
  `TOTAL_PRICE` double DEFAULT NULL,
  `PAID_AMOUNT` double DEFAULT NULL,
  `DUE_AMOUNT` double DEFAULT NULL,
  `ADVANCE_AMOUNT` double DEFAULT NULL,
  `NUMBER_OF_GUESTS` int(11) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `BAR_TAB` bit(1) DEFAULT NULL,
  `IS_TAX_EXEMPT` bit(1) DEFAULT NULL,
  `IS_RE_OPENED` bit(1) DEFAULT NULL,
  `SERVICE_CHARGE` double DEFAULT NULL,
  `DELIVERY_CHARGE` double DEFAULT NULL,
  `CUSTOMER_ID` int(11) DEFAULT NULL,
  `DELIVERY_ADDRESS` varchar(120) DEFAULT NULL,
  `CUSTOMER_PICKEUP` bit(1) DEFAULT NULL,
  `DELIVERY_EXTRA_INFO` varchar(255) DEFAULT NULL,
  `TICKET_TYPE` varchar(20) DEFAULT NULL,
  `SHIFT_ID` int(11) DEFAULT NULL,
  `OWNER_ID` int(11) DEFAULT NULL,
  `DRIVER_ID` int(11) DEFAULT NULL,
  `GRATUITY_ID` int(11) DEFAULT NULL,
  `VOID_BY_USER` int(11) DEFAULT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TICKET`
--

INSERT INTO `TICKET` (`ID`, `GLOBAL_ID`, `CREATE_DATE`, `CLOSING_DATE`, `ACTIVE_DATE`, `DELIVEERY_DATE`, `CREATION_HOUR`, `PAID`, `VOIDED`, `VOID_REASON`, `WASTED`, `REFUNDED`, `SETTLED`, `DRAWER_RESETTED`, `SUB_TOTAL`, `TOTAL_DISCOUNT`, `TOTAL_TAX`, `TOTAL_PRICE`, `PAID_AMOUNT`, `DUE_AMOUNT`, `ADVANCE_AMOUNT`, `NUMBER_OF_GUESTS`, `STATUS`, `BAR_TAB`, `IS_TAX_EXEMPT`, `IS_RE_OPENED`, `SERVICE_CHARGE`, `DELIVERY_CHARGE`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `CUSTOMER_PICKEUP`, `DELIVERY_EXTRA_INFO`, `TICKET_TYPE`, `SHIFT_ID`, `OWNER_ID`, `DRIVER_ID`, `GRATUITY_ID`, `VOID_BY_USER`, `TERMINAL_ID`) VALUES
(2, NULL, '2018-08-11 14:45:49', NULL, '2018-08-11 14:46:06', NULL, 14, b'0', b'0', NULL, b'0', b'0', b'0', b'0', 10, 0, 0.6, 10.6, 0, 10.6, 0, 1, '', b'0', b'0', b'0', 0, 0, 0, NULL, b'0', NULL, 'DINE IN', 1, 4, NULL, NULL, NULL, 7130);

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_DISCOUNT`
--

CREATE TABLE `TICKET_DISCOUNT` (
  `ID` int(11) NOT NULL,
  `DISCOUNT_ID` int(11) DEFAULT NULL,
  `NAME` varchar(30) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `AUTO_APPLY` bit(1) DEFAULT NULL,
  `MINIMUM_AMOUNT` int(11) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `TICKET_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_ITEM`
--

CREATE TABLE `TICKET_ITEM` (
  `ID` int(11) NOT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `ITEM_COUNT` int(11) DEFAULT NULL,
  `ITEM_QUANTITY` double DEFAULT NULL,
  `ITEM_NAME` varchar(120) DEFAULT NULL,
  `ITEM_UNIT_NAME` varchar(20) DEFAULT NULL,
  `GROUP_NAME` varchar(120) DEFAULT NULL,
  `CATEGORY_NAME` varchar(120) DEFAULT NULL,
  `ITEM_PRICE` double DEFAULT NULL,
  `ITEM_TAX_RATE` double DEFAULT NULL,
  `SUB_TOTAL` double DEFAULT NULL,
  `SUB_TOTAL_WITHOUT_MODIFIERS` double DEFAULT NULL,
  `DISCOUNT` double DEFAULT NULL,
  `TAX_AMOUNT` double DEFAULT NULL,
  `TAX_AMOUNT_WITHOUT_MODIFIERS` double DEFAULT NULL,
  `TOTAL_PRICE` double DEFAULT NULL,
  `TOTAL_PRICE_WITHOUT_MODIFIERS` double DEFAULT NULL,
  `BEVERAGE` bit(1) DEFAULT NULL,
  `INVENTORY_HANDLED` bit(1) DEFAULT NULL,
  `PRINT_TO_KITCHEN` bit(1) DEFAULT NULL,
  `TREAT_AS_SEAT` bit(1) DEFAULT NULL,
  `SEAT_NUMBER` int(11) DEFAULT NULL,
  `FRACTIONAL_UNIT` bit(1) DEFAULT NULL,
  `HAS_MODIIERS` bit(1) DEFAULT NULL,
  `PRINTED_TO_KITCHEN` bit(1) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `STOCK_AMOUNT_ADJUSTED` bit(1) DEFAULT NULL,
  `PIZZA_TYPE` bit(1) DEFAULT NULL,
  `SIZE_MODIFIER_ID` int(11) DEFAULT NULL,
  `TICKET_ID` int(11) DEFAULT NULL,
  `PG_ID` int(11) DEFAULT NULL,
  `PIZZA_SECTION_MODE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TICKET_ITEM`
--

INSERT INTO `TICKET_ITEM` (`ID`, `ITEM_ID`, `ITEM_COUNT`, `ITEM_QUANTITY`, `ITEM_NAME`, `ITEM_UNIT_NAME`, `GROUP_NAME`, `CATEGORY_NAME`, `ITEM_PRICE`, `ITEM_TAX_RATE`, `SUB_TOTAL`, `SUB_TOTAL_WITHOUT_MODIFIERS`, `DISCOUNT`, `TAX_AMOUNT`, `TAX_AMOUNT_WITHOUT_MODIFIERS`, `TOTAL_PRICE`, `TOTAL_PRICE_WITHOUT_MODIFIERS`, `BEVERAGE`, `INVENTORY_HANDLED`, `PRINT_TO_KITCHEN`, `TREAT_AS_SEAT`, `SEAT_NUMBER`, `FRACTIONAL_UNIT`, `HAS_MODIIERS`, `PRINTED_TO_KITCHEN`, `STATUS`, `STOCK_AMOUNT_ADJUSTED`, `PIZZA_TYPE`, `SIZE_MODIFIER_ID`, `TICKET_ID`, `PG_ID`, `PIZZA_SECTION_MODE`) VALUES
(5, 10, 1, 0, 'BELLYS SEASONAL', NULL, 'BEERS', 'BEER & WINE', 4, 6, 4, 4, 0, 0.24, 0.24, 4.24, 4.24, b'1', b'0', b'0', b'0', 0, b'0', b'0', b'0', NULL, b'1', b'0', NULL, 2, NULL, 0),
(6, 31, 1, 0, 'MICHELOB GOLDEN', NULL, 'BEERS', 'BEER & WINE', 2, 6, 2, 2, 0, 0.12, 0.12, 2.12, 2.12, b'1', b'0', b'0', b'0', 0, b'0', b'0', b'0', NULL, b'1', b'0', NULL, 2, NULL, 0),
(7, 37, 1, 0, 'SCHELLS FIREBRICH AMBER', NULL, 'BEERS', 'BEER & WINE', 4, 6, 4, 4, 0, 0.24, 0.24, 4.24, 4.24, b'1', b'0', b'0', b'0', 0, b'0', b'0', b'0', NULL, b'1', b'0', NULL, 2, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_ITEM_ADDON_RELATION`
--

CREATE TABLE `TICKET_ITEM_ADDON_RELATION` (
  `TICKET_ITEM_ID` int(11) NOT NULL,
  `MODIFIER_ID` int(11) NOT NULL,
  `LIST_ORDER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_ITEM_COOKING_INSTRUCTION`
--

CREATE TABLE `TICKET_ITEM_COOKING_INSTRUCTION` (
  `TICKET_ITEM_ID` int(11) NOT NULL,
  `description` varchar(60) DEFAULT NULL,
  `printedToKitchen` bit(1) DEFAULT NULL,
  `ITEM_ORDER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_ITEM_DISCOUNT`
--

CREATE TABLE `TICKET_ITEM_DISCOUNT` (
  `ID` int(11) NOT NULL,
  `DISCOUNT_ID` int(11) DEFAULT NULL,
  `NAME` varchar(30) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `AUTO_APPLY` bit(1) DEFAULT NULL,
  `MINIMUM_QUANTITY` int(11) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `TICKET_ITEMID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_ITEM_MODIFIER`
--

CREATE TABLE `TICKET_ITEM_MODIFIER` (
  `ID` int(11) NOT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `GROUP_ID` int(11) DEFAULT NULL,
  `ITEM_COUNT` int(11) DEFAULT NULL,
  `MODIFIER_NAME` varchar(120) DEFAULT NULL,
  `MODIFIER_PRICE` double DEFAULT NULL,
  `MODIFIER_TAX_RATE` double DEFAULT NULL,
  `MODIFIER_TYPE` int(11) DEFAULT NULL,
  `SUBTOTAL_PRICE` double DEFAULT NULL,
  `TOTAL_PRICE` double DEFAULT NULL,
  `TAX_AMOUNT` double DEFAULT NULL,
  `INFO_ONLY` bit(1) DEFAULT NULL,
  `SECTION_NAME` varchar(20) DEFAULT NULL,
  `MULTIPLIER_NAME` varchar(20) DEFAULT NULL,
  `PRINT_TO_KITCHEN` bit(1) DEFAULT NULL,
  `SECTION_WISE_PRICING` bit(1) DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `PRINTED_TO_KITCHEN` bit(1) DEFAULT NULL,
  `TICKET_ITEM_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_ITEM_MODIFIER_RELATION`
--

CREATE TABLE `TICKET_ITEM_MODIFIER_RELATION` (
  `TICKET_ITEM_ID` int(11) NOT NULL,
  `MODIFIER_ID` int(11) NOT NULL,
  `LIST_ORDER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_PROPERTIES`
--

CREATE TABLE `TICKET_PROPERTIES` (
  `id` int(11) NOT NULL,
  `property_value` text,
  `property_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TICKET_TABLE_NUM`
--

CREATE TABLE `TICKET_TABLE_NUM` (
  `ticket_id` int(11) NOT NULL,
  `TABLE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TICKET_TABLE_NUM`
--

INSERT INTO `TICKET_TABLE_NUM` (`ticket_id`, `TABLE_ID`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `TRANSACTIONS`
--

CREATE TABLE `TRANSACTIONS` (
  `ID` int(11) NOT NULL,
  `PAYMENT_TYPE` varchar(30) NOT NULL,
  `GLOBAL_ID` varchar(16) DEFAULT NULL,
  `TRANSACTION_TIME` datetime DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `TIPS_AMOUNT` double DEFAULT NULL,
  `TIPS_EXCEED_AMOUNT` double DEFAULT NULL,
  `TENDER_AMOUNT` double DEFAULT NULL,
  `TRANSACTION_TYPE` varchar(30) NOT NULL,
  `CUSTOM_PAYMENT_NAME` varchar(60) DEFAULT NULL,
  `CUSTOM_PAYMENT_REF` varchar(120) DEFAULT NULL,
  `CUSTOM_PAYMENT_FIELD_NAME` varchar(60) DEFAULT NULL,
  `PAYMENT_SUB_TYPE` varchar(40) NOT NULL,
  `CAPTURED` bit(1) DEFAULT NULL,
  `VOIDED` bit(1) DEFAULT NULL,
  `AUTHORIZABLE` bit(1) DEFAULT NULL,
  `CARD_HOLDER_NAME` varchar(60) DEFAULT NULL,
  `CARD_NUMBER` varchar(40) DEFAULT NULL,
  `CARD_AUTH_CODE` varchar(30) DEFAULT NULL,
  `CARD_TYPE` varchar(20) DEFAULT NULL,
  `CARD_TRANSACTION_ID` varchar(255) DEFAULT NULL,
  `CARD_MERCHANT_GATEWAY` varchar(60) DEFAULT NULL,
  `CARD_READER` varchar(30) DEFAULT NULL,
  `CARD_AID` varchar(120) DEFAULT NULL,
  `CARD_ARQC` varchar(120) DEFAULT NULL,
  `CARD_EXT_DATA` varchar(255) DEFAULT NULL,
  `GIFT_CERT_NUMBER` varchar(64) DEFAULT NULL,
  `GIFT_CERT_FACE_VALUE` double DEFAULT NULL,
  `GIFT_CERT_PAID_AMOUNT` double DEFAULT NULL,
  `GIFT_CERT_CASH_BACK_AMOUNT` double DEFAULT NULL,
  `DRAWER_RESETTED` bit(1) DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `TERMINAL_ID` int(11) DEFAULT NULL,
  `TICKET_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `PAYOUT_REASON_ID` int(11) DEFAULT NULL,
  `PAYOUT_RECEPIENT_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TRANSACTION_PROPERTIES`
--

CREATE TABLE `TRANSACTION_PROPERTIES` (
  `id` int(11) NOT NULL,
  `property_value` varchar(255) DEFAULT NULL,
  `property_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--

CREATE TABLE `USERS` (
  `AUTO_ID` int(11) NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `USER_PASS` varchar(16) NOT NULL,
  `FIRST_NAME` varchar(30) DEFAULT NULL,
  `LAST_NAME` varchar(30) DEFAULT NULL,
  `SSN` varchar(30) DEFAULT NULL,
  `COST_PER_HOUR` double DEFAULT NULL,
  `CLOCKED_IN` bit(1) DEFAULT NULL,
  `LAST_CLOCK_IN_TIME` datetime DEFAULT NULL,
  `LAST_CLOCK_OUT_TIME` datetime DEFAULT NULL,
  `PHONE_NO` varchar(20) DEFAULT NULL,
  `IS_DRIVER` bit(1) DEFAULT NULL,
  `AVAILABLE_FOR_DELIVERY` bit(1) DEFAULT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `SHIFT_ID` int(11) DEFAULT NULL,
  `currentTerminal` int(11) DEFAULT NULL,
  `N_USER_TYPE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`AUTO_ID`, `USER_ID`, `USER_PASS`, `FIRST_NAME`, `LAST_NAME`, `SSN`, `COST_PER_HOUR`, `CLOCKED_IN`, `LAST_CLOCK_IN_TIME`, `LAST_CLOCK_OUT_TIME`, `PHONE_NO`, `IS_DRIVER`, `AVAILABLE_FOR_DELIVERY`, `ACTIVE`, `SHIFT_ID`, `currentTerminal`, `N_USER_TYPE`) VALUES
(1, 123, '1111', 'Admin', 'System', '123', 0, b'1', '2018-08-11 14:44:46', NULL, NULL, b'0', b'0', b'1', 1, 7130, 1),
(2, 124, '2222', 'Lisa', 'Carol', '124', 0, b'0', NULL, NULL, NULL, b'0', b'0', b'1', NULL, NULL, 2),
(3, 125, '3333', 'Janet', 'Ann', '125', 0, b'1', '2018-08-11 14:43:27', NULL, NULL, b'0', b'0', b'1', 1, 7130, 3),
(4, 126, '7777', 'John', 'Doe', '126', 0, b'1', '2018-08-11 14:45:47', NULL, NULL, b'0', b'0', b'1', 1, 7130, 4),
(5, 127, '8888', 'Poll', 'Brien', '127', 0, b'0', NULL, NULL, NULL, b'1', b'0', b'1', NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `USER_PERMISSION`
--

CREATE TABLE `USER_PERMISSION` (
  `NAME` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `USER_PERMISSION`
--

INSERT INTO `USER_PERMISSION` (`NAME`) VALUES
('Add Discount'),
('All Functions'),
('Authorize Tickets'),
('Booking'),
('Create New Ticket'),
('Drawer Assignment'),
('Drawer Pull'),
('Hold Ticket'),
('Kitchen Display'),
('Manage Table Layout'),
('Modify Printed Ticket'),
('Pay Out'),
('Perform Administrative Task'),
('Perform Manager Task'),
('Quick Maintenance'),
('Refund'),
('Reopen Ticket'),
('Settle Ticket'),
('Shut Down'),
('Split Ticket'),
('Transfer Ticket'),
('View All Close Tickets'),
('View All Open Ticket'),
('View Back Office'),
('View Explorers'),
('View Reports'),
('Void Ticket');

-- --------------------------------------------------------

--
-- Table structure for table `USER_TYPE`
--

CREATE TABLE `USER_TYPE` (
  `ID` int(11) NOT NULL,
  `P_NAME` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `USER_TYPE`
--

INSERT INTO `USER_TYPE` (`ID`, `P_NAME`) VALUES
(1, 'Administrator'),
(2, 'MANAGER'),
(3, 'CASHIER'),
(4, 'SR. CASHIER');

-- --------------------------------------------------------

--
-- Table structure for table `USER_USER_PERMISSION`
--

CREATE TABLE `USER_USER_PERMISSION` (
  `permissionId` int(11) NOT NULL,
  `elt` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `USER_USER_PERMISSION`
--

INSERT INTO `USER_USER_PERMISSION` (`permissionId`, `elt`) VALUES
(1, 'Add Discount'),
(1, 'All Functions'),
(1, 'Authorize Tickets'),
(1, 'Booking'),
(1, 'Create New Ticket'),
(1, 'Drawer Assignment'),
(1, 'Drawer Pull'),
(1, 'Hold Ticket'),
(1, 'Kitchen Display'),
(1, 'Manage Table Layout'),
(1, 'Modify Printed Ticket'),
(1, 'Pay Out'),
(1, 'Perform Administrative Task'),
(1, 'Perform Manager Task'),
(1, 'Quick Maintenance'),
(1, 'Refund'),
(1, 'Reopen Ticket'),
(1, 'Settle Ticket'),
(1, 'Shut Down'),
(1, 'Split Ticket'),
(1, 'Transfer Ticket'),
(1, 'View All Close Tickets'),
(1, 'View All Open Ticket'),
(1, 'View Back Office'),
(1, 'View Explorers'),
(1, 'View Reports'),
(1, 'Void Ticket'),
(2, 'Add Discount'),
(2, 'All Functions'),
(2, 'Authorize Tickets'),
(2, 'Booking'),
(2, 'Create New Ticket'),
(2, 'Drawer Assignment'),
(2, 'Drawer Pull'),
(2, 'Hold Ticket'),
(2, 'Kitchen Display'),
(2, 'Manage Table Layout'),
(2, 'Modify Printed Ticket'),
(2, 'Pay Out'),
(2, 'Perform Administrative Task'),
(2, 'Perform Manager Task'),
(2, 'Quick Maintenance'),
(2, 'Refund'),
(2, 'Reopen Ticket'),
(2, 'Settle Ticket'),
(2, 'Shut Down'),
(2, 'Split Ticket'),
(2, 'Transfer Ticket'),
(2, 'View All Close Tickets'),
(2, 'View All Open Ticket'),
(2, 'View Back Office'),
(2, 'View Explorers'),
(2, 'View Reports'),
(2, 'Void Ticket'),
(3, 'Create New Ticket'),
(3, 'Settle Ticket'),
(3, 'Split Ticket'),
(3, 'View All Open Ticket'),
(4, 'Create New Ticket'),
(4, 'Settle Ticket'),
(4, 'Split Ticket');

-- --------------------------------------------------------

--
-- Table structure for table `VIRTUALPRINTER_ORDER_TYPE`
--

CREATE TABLE `VIRTUALPRINTER_ORDER_TYPE` (
  `printer_id` int(11) NOT NULL,
  `ORDER_TYPE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `VIRTUAL_PRINTER`
--

CREATE TABLE `VIRTUAL_PRINTER` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(60) NOT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `ENABLED` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `VOID_REASONS`
--

CREATE TABLE `VOID_REASONS` (
  `ID` int(11) NOT NULL,
  `REASON_TEXT` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ZIP_CODE_VS_DELIVERY_CHARGE`
--

CREATE TABLE `ZIP_CODE_VS_DELIVERY_CHARGE` (
  `AUTO_ID` int(11) NOT NULL,
  `ZIP_CODE` varchar(10) NOT NULL,
  `DELIVERY_CHARGE` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ACTION_HISTORY`
--
ALTER TABLE `ACTION_HISTORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK3F3AF36B3E20AD51` (`USER_ID`);

--
-- Indexes for table `ATTENDENCE_HISTORY`
--
ALTER TABLE `ATTENDENCE_HISTORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKDFE829A3E20AD51` (`USER_ID`),
  ADD KEY `FKDFE829A7660A5E3` (`SHIFT_ID`),
  ADD KEY `FKDFE829A2AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `CASH_DRAWER`
--
ALTER TABLE `CASH_DRAWER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK6221077D2AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `CASH_DRAWER_RESET_HISTORY`
--
ALTER TABLE `CASH_DRAWER_RESET_HISTORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK719418223E20AD51` (`USER_ID`);

--
-- Indexes for table `COOKING_INSTRUCTION`
--
ALTER TABLE `COOKING_INSTRUCTION`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `COUPON_AND_DISCOUNT`
--
ALTER TABLE `COUPON_AND_DISCOUNT`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UUID` (`UUID`);

--
-- Indexes for table `CURRENCY`
--
ALTER TABLE `CURRENCY`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `CURRENCY_BALANCE`
--
ALTER TABLE `CURRENCY_BALANCE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK2CC0E08EFB910735` (`DPR_ID`),
  ADD KEY `FK2CC0E08E9006558` (`CASH_DRAWER_ID`),
  ADD KEY `FK2CC0E08E28DD6C11` (`CURRENCY_ID`);

--
-- Indexes for table `CUSTOMER`
--
ALTER TABLE `CUSTOMER`
  ADD PRIMARY KEY (`AUTO_ID`);

--
-- Indexes for table `CUSTOMER_PROPERTIES`
--
ALTER TABLE `CUSTOMER_PROPERTIES`
  ADD PRIMARY KEY (`id`,`property_name`),
  ADD KEY `FKD43068347BBCCF0` (`id`);

--
-- Indexes for table `CUSTOM_PAYMENT`
--
ALTER TABLE `CUSTOM_PAYMENT`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `DATA_UPDATE_INFO`
--
ALTER TABLE `DATA_UPDATE_INFO`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `DELIVERY_ADDRESS`
--
ALTER TABLE `DELIVERY_ADDRESS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK29ACA6899E1C3CF1` (`CUSTOMER_ID`);

--
-- Indexes for table `DELIVERY_CHARGE`
--
ALTER TABLE `DELIVERY_CHARGE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `DELIVERY_CONFIGURATION`
--
ALTER TABLE `DELIVERY_CONFIGURATION`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `DELIVERY_INSTRUCTION`
--
ALTER TABLE `DELIVERY_INSTRUCTION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK29D9CA39E1C3D97` (`CUSTOMER_NO`);

--
-- Indexes for table `DRAWER_ASSIGNED_HISTORY`
--
ALTER TABLE `DRAWER_ASSIGNED_HISTORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK5A823C91F1DD782B` (`A_USER`);

--
-- Indexes for table `DRAWER_PULL_REPORT`
--
ALTER TABLE `DRAWER_PULL_REPORT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `drawer_report_time` (`REPORT_TIME`),
  ADD KEY `FKAEC362203E20AD51` (`USER_ID`),
  ADD KEY `FKAEC362202AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `DRAWER_PULL_REPORT_VOIDTICKETS`
--
ALTER TABLE `DRAWER_PULL_REPORT_VOIDTICKETS`
  ADD KEY `FK98CF9B143EF4CD9B` (`DPREPORT_ID`);

--
-- Indexes for table `EMPLOYEE_IN_OUT_HISTORY`
--
ALTER TABLE `EMPLOYEE_IN_OUT_HISTORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK6D5DB9FA3E20AD51` (`USER_ID`),
  ADD KEY `FK6D5DB9FA7660A5E3` (`SHIFT_ID`),
  ADD KEY `FK6D5DB9FA2AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `GLOBAL_CONFIG`
--
ALTER TABLE `GLOBAL_CONFIG`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `POS_KEY` (`POS_KEY`);

--
-- Indexes for table `GRATUITY`
--
ALTER TABLE `GRATUITY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK34E4E377AA075D69` (`OWNER_ID`),
  ADD KEY `FK34E4E3771DF2D7F1` (`TICKET_ID`),
  ADD KEY `FK34E4E3772AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `INVENTORY_GROUP`
--
ALTER TABLE `INVENTORY_GROUP`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `INVENTORY_ITEM`
--
ALTER TABLE `INVENTORY_ITEM`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK7DC968369E60C333` (`ITEM_VENDOR_ID`),
  ADD KEY `FK7DC968366848D615` (`RECIPE_UNIT_ID`),
  ADD KEY `FK7DC968362CD583C1` (`ITEM_GROUP_ID`),
  ADD KEY `FK7DC968363525E956` (`PUNIT_ID`),
  ADD KEY `FK7DC9683695E455D3` (`ITEM_LOCATION_ID`);

--
-- Indexes for table `INVENTORY_LOCATION`
--
ALTER TABLE `INVENTORY_LOCATION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK59073B58C46A9C15` (`WAREHOUSE_ID`);

--
-- Indexes for table `INVENTORY_META_CODE`
--
ALTER TABLE `INVENTORY_META_CODE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `INVENTORY_TRANSACTION`
--
ALTER TABLE `INVENTORY_TRANSACTION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKAF48F43BFF3F328A` (`FROM_WAREHOUSE_ID`),
  ADD KEY `FKAF48F43B5B397C5` (`REFERENCE_ID`),
  ADD KEY `FKAF48F43B96A3D6BF` (`ITEM_ID`),
  ADD KEY `FKAF48F43BEDA09759` (`TO_WAREHOUSE_ID`),
  ADD KEY `FKAF48F43BD152C95F` (`VENDOR_ID`);

--
-- Indexes for table `INVENTORY_UNIT`
--
ALTER TABLE `INVENTORY_UNIT`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `INVENTORY_VENDOR`
--
ALTER TABLE `INVENTORY_VENDOR`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `INVENTORY_WAREHOUSE`
--
ALTER TABLE `INVENTORY_WAREHOUSE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ITEM_ORDER_TYPE`
--
ALTER TABLE `ITEM_ORDER_TYPE`
  ADD KEY `FKE2B8465789FE23F0` (`MENU_ITEM_ID`),
  ADD KEY `FKE2B846573AC1D2E0` (`ORDER_TYPE_ID`);

--
-- Indexes for table `KITCHEN_TICKET`
--
ALTER TABLE `KITCHEN_TICKET`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK341CBC275CF1375F` (`PG_ID`);

--
-- Indexes for table `KITCHEN_TICKET_ITEM`
--
ALTER TABLE `KITCHEN_TICKET_ITEM`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1462F02BCB07FAA3` (`KITHEN_TICKET_ID`);

--
-- Indexes for table `KIT_TICKET_TABLE_NUM`
--
ALTER TABLE `KIT_TICKET_TABLE_NUM`
  ADD KEY `FK5696584BB73E273E` (`kit_ticket_id`);

--
-- Indexes for table `MENUCATEGORY_DISCOUNT`
--
ALTER TABLE `MENUCATEGORY_DISCOUNT`
  ADD KEY `FK4F8523E38D9EA931` (`MENUCATEGORY_ID`),
  ADD KEY `FK4F8523E3D3E91E11` (`DISCOUNT_ID`);

--
-- Indexes for table `MENUGROUP_DISCOUNT`
--
ALTER TABLE `MENUGROUP_DISCOUNT`
  ADD KEY `FKE3790E40113BF083` (`MENUGROUP_ID`),
  ADD KEY `FKE3790E40D3E91E11` (`DISCOUNT_ID`);

--
-- Indexes for table `MENUITEM_DISCOUNT`
--
ALTER TABLE `MENUITEM_DISCOUNT`
  ADD KEY `FKD89CCDEE33662891` (`MENUITEM_ID`),
  ADD KEY `FKD89CCDEED3E91E11` (`DISCOUNT_ID`);

--
-- Indexes for table `MENUITEM_MODIFIERGROUP`
--
ALTER TABLE `MENUITEM_MODIFIERGROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK312B355B40FDA3C9` (`MODIFIER_GROUP`),
  ADD KEY `FK312B355B6E7B8B68` (`MENUITEM_MODIFIERGROUP_ID`);

--
-- Indexes for table `MENUITEM_PIZZAPIRCE`
--
ALTER TABLE `MENUITEM_PIZZAPIRCE`
  ADD KEY `FK17BD51A089FE23F0` (`MENU_ITEM_ID`),
  ADD KEY `FK17BD51A0AE5D580` (`PIZZA_PRICE_ID`);

--
-- Indexes for table `MENUITEM_SHIFT`
--
ALTER TABLE `MENUITEM_SHIFT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKE03C92D533662891` (`MENUITEM_ID`),
  ADD KEY `FKE03C92D57660A5E3` (`SHIFT_ID`);

--
-- Indexes for table `MENUMODIFIER_PIZZAMODIFIERPRICE`
--
ALTER TABLE `MENUMODIFIER_PIZZAMODIFIERPRICE`
  ADD KEY `FK572726F374BE2C71` (`PIZZAMODIFIERPRICE_ID`),
  ADD KEY `FK572726F3AE3F2E91` (`MENUMODIFIER_ID`);

--
-- Indexes for table `MENU_CATEGORY`
--
ALTER TABLE `MENU_CATEGORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `food_category_visible` (`VISIBLE`);

--
-- Indexes for table `MENU_GROUP`
--
ALTER TABLE `MENU_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `menugroupvisible` (`VISIBLE`),
  ADD KEY `FK4DC1AB7F2E347FF0` (`CATEGORY_ID`);

--
-- Indexes for table `MENU_ITEM`
--
ALTER TABLE `MENU_ITEM`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK4CD5A1F35188AA24` (`GROUP_ID`),
  ADD KEY `FK4CD5A1F3F3B77C57` (`RECEPIE`),
  ADD KEY `FK4CD5A1F35CF1375F` (`PG_ID`),
  ADD KEY `FK4CD5A1F3A4802F83` (`TAX_ID`);

--
-- Indexes for table `MENU_ITEM_PROPERTIES`
--
ALTER TABLE `MENU_ITEM_PROPERTIES`
  ADD PRIMARY KEY (`MENU_ITEM_ID`,`PROPERTY_NAME`),
  ADD KEY `FKF94186FF89FE23F0` (`MENU_ITEM_ID`);

--
-- Indexes for table `MENU_ITEM_SIZE`
--
ALTER TABLE `MENU_ITEM_SIZE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `MENU_ITEM_TERMINAL_REF`
--
ALTER TABLE `MENU_ITEM_TERMINAL_REF`
  ADD KEY `FK9EA1AFC89FE23F0` (`MENU_ITEM_ID`),
  ADD KEY `FK9EA1AFC2AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `MENU_MODIFIER`
--
ALTER TABLE `MENU_MODIFIER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `modifierenabled` (`ENABLE`),
  ADD KEY `FK59B6B1B75E0C7B8D` (`GROUP_ID`),
  ADD KEY `FK59B6B1B7A4802F83` (`TAX_ID`);

--
-- Indexes for table `MENU_MODIFIER_GROUP`
--
ALTER TABLE `MENU_MODIFIER_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `mg_enable` (`ENABLED`);

--
-- Indexes for table `MENU_MODIFIER_PROPERTIES`
--
ALTER TABLE `MENU_MODIFIER_PROPERTIES`
  ADD PRIMARY KEY (`MENU_MODIFIER_ID`,`PROPERTY_NAME`),
  ADD KEY `FK1273B4BBB79C6270` (`MENU_MODIFIER_ID`);

--
-- Indexes for table `MODIFIER_MULTIPLIER_PRICE`
--
ALTER TABLE `MODIFIER_MULTIPLIER_PRICE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK8A1609939C9E4883` (`PIZZA_MODIFIER_PRICE_ID`),
  ADD KEY `FK8A160993AE3F2E91` (`MENUMODIFIER_ID`),
  ADD KEY `FK8A16099391D62C51` (`MULTIPLIER_ID`);

--
-- Indexes for table `MULTIPLIER`
--
ALTER TABLE `MULTIPLIER`
  ADD PRIMARY KEY (`NAME`);

--
-- Indexes for table `ORDER_TYPE`
--
ALTER TABLE `ORDER_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `PACKAGING_UNIT`
--
ALTER TABLE `PACKAGING_UNIT`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `PAYOUT_REASONS`
--
ALTER TABLE `PAYOUT_REASONS`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `PAYOUT_RECEPIENTS`
--
ALTER TABLE `PAYOUT_RECEPIENTS`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `PIZZA_CRUST`
--
ALTER TABLE `PIZZA_CRUST`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `PIZZA_MODIFIER_PRICE`
--
ALTER TABLE `PIZZA_MODIFIER_PRICE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKD3DE7E7896183657` (`ITEM_SIZE`);

--
-- Indexes for table `PIZZA_PRICE`
--
ALTER TABLE `PIZZA_PRICE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKEAC11292DD545B77` (`MENU_ITEM_SIZE`),
  ADD KEY `FKEAC11292A56D141C` (`ORDER_TYPE`),
  ADD KEY `FKEAC112927C59441D` (`CRUST`);

--
-- Indexes for table `PRINTER_CONFIGURATION`
--
ALTER TABLE `PRINTER_CONFIGURATION`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `PRINTER_GROUP`
--
ALTER TABLE `PRINTER_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `PRINTER_GROUP_PRINTERS`
--
ALTER TABLE `PRINTER_GROUP_PRINTERS`
  ADD KEY `FKC05B805E5F31265C` (`printer_id`);

--
-- Indexes for table `PURCHASE_ORDER`
--
ALTER TABLE `PURCHASE_ORDER`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `RECEPIE`
--
ALTER TABLE `RECEPIE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK6B4E177764931EFC` (`MENU_ITEM`);

--
-- Indexes for table `RECEPIE_ITEM`
--
ALTER TABLE `RECEPIE_ITEM`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK855626DBCAE89B83` (`RECEPIE_ID`),
  ADD KEY `FK855626DB1682B10E` (`INVENTORY_ITEM`);

--
-- Indexes for table `RESTAURANT`
--
ALTER TABLE `RESTAURANT`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `SHIFT`
--
ALTER TABLE `SHIFT`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `SHOP_FLOOR`
--
ALTER TABLE `SHOP_FLOOR`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `SHOP_FLOOR_TEMPLATE`
--
ALTER TABLE `SHOP_FLOOR_TEMPLATE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKBA6EFBD68979C3CD` (`FLOOR_ID`);

--
-- Indexes for table `SHOP_FLOOR_TEMPLATE_PROPERTIES`
--
ALTER TABLE `SHOP_FLOOR_TEMPLATE_PROPERTIES`
  ADD PRIMARY KEY (`id`,`property_name`),
  ADD KEY `FKD70C313CA36AB054` (`id`);

--
-- Indexes for table `SHOP_TABLE`
--
ALTER TABLE `SHOP_TABLE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK2458E9258979C3CD` (`FLOOR_ID`);

--
-- Indexes for table `SHOP_TABLE_TYPE`
--
ALTER TABLE `SHOP_TABLE_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `TABLE_BOOKING_INFO`
--
ALTER TABLE `TABLE_BOOKING_INFO`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fromDate` (`FROM_DATE`),
  ADD KEY `toDate` (`TO_DATE`),
  ADD KEY `FK301C4DE53E20AD51` (`user_id`),
  ADD KEY `FK301C4DE59E1C3CF1` (`customer_id`);

--
-- Indexes for table `TABLE_BOOKING_MAPPING`
--
ALTER TABLE `TABLE_BOOKING_MAPPING`
  ADD KEY `FK6BC51417160DE3B1` (`BOOKING_ID`),
  ADD KEY `FK6BC51417DC46948D` (`TABLE_ID`);

--
-- Indexes for table `TABLE_TYPE_RELATION`
--
ALTER TABLE `TABLE_TYPE_RELATION`
  ADD KEY `FK93802290F5D6E47B` (`TYPE_ID`),
  ADD KEY `FK93802290DC46948D` (`TABLE_ID`);

--
-- Indexes for table `TAX`
--
ALTER TABLE `TAX`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `TERMINAL`
--
ALTER TABLE `TERMINAL`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKE83D827C969C6DE` (`ASSIGNED_USER`);

--
-- Indexes for table `TERMINAL_PRINTERS`
--
ALTER TABLE `TERMINAL_PRINTERS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK99EDE5FCC433E65A` (`VIRTUAL_PRINTER_ID`),
  ADD KEY `FK99EDE5FC2AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `TICKET`
--
ALTER TABLE `TICKET`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `GLOBAL_ID` (`GLOBAL_ID`),
  ADD KEY `creationHour` (`CREATION_HOUR`),
  ADD KEY `ticketpaid` (`PAID`),
  ADD KEY `ticketactiveDate` (`ACTIVE_DATE`),
  ADD KEY `ticketcreateDate` (`CREATE_DATE`),
  ADD KEY `ticketsettled` (`SETTLED`),
  ADD KEY `deliveryDate` (`DELIVEERY_DATE`),
  ADD KEY `ticketclosingDate` (`CLOSING_DATE`),
  ADD KEY `drawerresetted` (`DRAWER_RESETTED`),
  ADD KEY `ticketvoided` (`VOIDED`),
  ADD KEY `FK937B5F0CF575C7D4` (`DRIVER_ID`),
  ADD KEY `FK937B5F0CAA075D69` (`OWNER_ID`),
  ADD KEY `FK937B5F0C7660A5E3` (`SHIFT_ID`),
  ADD KEY `FK937B5F0CC188EA51` (`GRATUITY_ID`),
  ADD KEY `FK937B5F0C1F6A9A4A` (`VOID_BY_USER`),
  ADD KEY `FK937B5F0C2AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `TICKET_DISCOUNT`
--
ALTER TABLE `TICKET_DISCOUNT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1FA465141DF2D7F1` (`TICKET_ID`);

--
-- Indexes for table `TICKET_ITEM`
--
ALTER TABLE `TICKET_ITEM`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK979F546633E5D3B2` (`SIZE_MODIFIER_ID`),
  ADD KEY `FK979F54665CF1375F` (`PG_ID`),
  ADD KEY `FK979F54661DF2D7F1` (`TICKET_ID`);

--
-- Indexes for table `TICKET_ITEM_ADDON_RELATION`
--
ALTER TABLE `TICKET_ITEM_ADDON_RELATION`
  ADD PRIMARY KEY (`TICKET_ITEM_ID`,`LIST_ORDER`),
  ADD KEY `FK9F199634DEC6120A` (`TICKET_ITEM_ID`),
  ADD KEY `FK9F1996346C108EF0` (`MODIFIER_ID`);

--
-- Indexes for table `TICKET_ITEM_COOKING_INSTRUCTION`
--
ALTER TABLE `TICKET_ITEM_COOKING_INSTRUCTION`
  ADD PRIMARY KEY (`TICKET_ITEM_ID`,`ITEM_ORDER`),
  ADD KEY `FK3825F9D0DEC6120A` (`TICKET_ITEM_ID`);

--
-- Indexes for table `TICKET_ITEM_DISCOUNT`
--
ALTER TABLE `TICKET_ITEM_DISCOUNT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK3DF5D4FAB9276E77` (`TICKET_ITEMID`);

--
-- Indexes for table `TICKET_ITEM_MODIFIER`
--
ALTER TABLE `TICKET_ITEM_MODIFIER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK8FD6290DEC6120A` (`TICKET_ITEM_ID`);

--
-- Indexes for table `TICKET_ITEM_MODIFIER_RELATION`
--
ALTER TABLE `TICKET_ITEM_MODIFIER_RELATION`
  ADD PRIMARY KEY (`TICKET_ITEM_ID`,`LIST_ORDER`),
  ADD KEY `FK5D3F9ACBDEC6120A` (`TICKET_ITEM_ID`),
  ADD KEY `FK5D3F9ACB6C108EF0` (`MODIFIER_ID`);

--
-- Indexes for table `TICKET_PROPERTIES`
--
ALTER TABLE `TICKET_PROPERTIES`
  ADD PRIMARY KEY (`id`,`property_name`),
  ADD KEY `FK70ECD046223049DE` (`id`);

--
-- Indexes for table `TICKET_TABLE_NUM`
--
ALTER TABLE `TICKET_TABLE_NUM`
  ADD KEY `FK65AF15E21DF2D7F1` (`ticket_id`);

--
-- Indexes for table `TRANSACTIONS`
--
ALTER TABLE `TRANSACTIONS`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `GLOBAL_ID` (`GLOBAL_ID`),
  ADD KEY `tran_drawer_resetted` (`DRAWER_RESETTED`),
  ADD KEY `FKFE9871553E20AD51` (`USER_ID`),
  ADD KEY `FKFE987155FC697D9E` (`PAYOUT_REASON_ID`),
  ADD KEY `FKFE987155CA43B6` (`PAYOUT_RECEPIENT_ID`),
  ADD KEY `FKFE9871551DF2D7F1` (`TICKET_ID`),
  ADD KEY `FKFE9871552AD2D031` (`TERMINAL_ID`);

--
-- Indexes for table `TRANSACTION_PROPERTIES`
--
ALTER TABLE `TRANSACTION_PROPERTIES`
  ADD PRIMARY KEY (`id`,`property_name`),
  ADD KEY `FKE3DE65548E8203BC` (`id`);

--
-- Indexes for table `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`AUTO_ID`),
  ADD UNIQUE KEY `USER_PASS` (`USER_PASS`),
  ADD UNIQUE KEY `USER_ID` (`USER_ID`),
  ADD KEY `FK4D495E87660A5E3` (`SHIFT_ID`),
  ADD KEY `FK4D495E8D9409968` (`currentTerminal`),
  ADD KEY `FK4D495E8897B1E39` (`N_USER_TYPE`);

--
-- Indexes for table `USER_PERMISSION`
--
ALTER TABLE `USER_PERMISSION`
  ADD PRIMARY KEY (`NAME`);

--
-- Indexes for table `USER_TYPE`
--
ALTER TABLE `USER_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `USER_USER_PERMISSION`
--
ALTER TABLE `USER_USER_PERMISSION`
  ADD PRIMARY KEY (`permissionId`,`elt`),
  ADD KEY `FK2DBEAA4F283ECC6` (`permissionId`),
  ADD KEY `FK2DBEAA4F8F23F5E` (`elt`);

--
-- Indexes for table `VIRTUALPRINTER_ORDER_TYPE`
--
ALTER TABLE `VIRTUALPRINTER_ORDER_TYPE`
  ADD KEY `FK9AF7853BCF15F4A6` (`printer_id`);

--
-- Indexes for table `VIRTUAL_PRINTER`
--
ALTER TABLE `VIRTUAL_PRINTER`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `VOID_REASONS`
--
ALTER TABLE `VOID_REASONS`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ZIP_CODE_VS_DELIVERY_CHARGE`
--
ALTER TABLE `ZIP_CODE_VS_DELIVERY_CHARGE`
  ADD PRIMARY KEY (`AUTO_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ACTION_HISTORY`
--
ALTER TABLE `ACTION_HISTORY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ATTENDENCE_HISTORY`
--
ALTER TABLE `ATTENDENCE_HISTORY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `CASH_DRAWER`
--
ALTER TABLE `CASH_DRAWER`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `CASH_DRAWER_RESET_HISTORY`
--
ALTER TABLE `CASH_DRAWER_RESET_HISTORY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `COOKING_INSTRUCTION`
--
ALTER TABLE `COOKING_INSTRUCTION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `COUPON_AND_DISCOUNT`
--
ALTER TABLE `COUPON_AND_DISCOUNT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `CURRENCY`
--
ALTER TABLE `CURRENCY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `CURRENCY_BALANCE`
--
ALTER TABLE `CURRENCY_BALANCE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `CUSTOMER`
--
ALTER TABLE `CUSTOMER`
  MODIFY `AUTO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `CUSTOM_PAYMENT`
--
ALTER TABLE `CUSTOM_PAYMENT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `DATA_UPDATE_INFO`
--
ALTER TABLE `DATA_UPDATE_INFO`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `DELIVERY_ADDRESS`
--
ALTER TABLE `DELIVERY_ADDRESS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `DELIVERY_CHARGE`
--
ALTER TABLE `DELIVERY_CHARGE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `DELIVERY_CONFIGURATION`
--
ALTER TABLE `DELIVERY_CONFIGURATION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `DELIVERY_INSTRUCTION`
--
ALTER TABLE `DELIVERY_INSTRUCTION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `DRAWER_ASSIGNED_HISTORY`
--
ALTER TABLE `DRAWER_ASSIGNED_HISTORY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `DRAWER_PULL_REPORT`
--
ALTER TABLE `DRAWER_PULL_REPORT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `EMPLOYEE_IN_OUT_HISTORY`
--
ALTER TABLE `EMPLOYEE_IN_OUT_HISTORY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GLOBAL_CONFIG`
--
ALTER TABLE `GLOBAL_CONFIG`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GRATUITY`
--
ALTER TABLE `GRATUITY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_GROUP`
--
ALTER TABLE `INVENTORY_GROUP`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_ITEM`
--
ALTER TABLE `INVENTORY_ITEM`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_LOCATION`
--
ALTER TABLE `INVENTORY_LOCATION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_META_CODE`
--
ALTER TABLE `INVENTORY_META_CODE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_TRANSACTION`
--
ALTER TABLE `INVENTORY_TRANSACTION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_UNIT`
--
ALTER TABLE `INVENTORY_UNIT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_VENDOR`
--
ALTER TABLE `INVENTORY_VENDOR`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `INVENTORY_WAREHOUSE`
--
ALTER TABLE `INVENTORY_WAREHOUSE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `KITCHEN_TICKET`
--
ALTER TABLE `KITCHEN_TICKET`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `KITCHEN_TICKET_ITEM`
--
ALTER TABLE `KITCHEN_TICKET_ITEM`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `MENUITEM_MODIFIERGROUP`
--
ALTER TABLE `MENUITEM_MODIFIERGROUP`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `MENUITEM_SHIFT`
--
ALTER TABLE `MENUITEM_SHIFT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `MENU_CATEGORY`
--
ALTER TABLE `MENU_CATEGORY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `MENU_GROUP`
--
ALTER TABLE `MENU_GROUP`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `MENU_ITEM`
--
ALTER TABLE `MENU_ITEM`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `MENU_ITEM_SIZE`
--
ALTER TABLE `MENU_ITEM_SIZE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `MENU_MODIFIER`
--
ALTER TABLE `MENU_MODIFIER`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `MENU_MODIFIER_GROUP`
--
ALTER TABLE `MENU_MODIFIER_GROUP`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `MODIFIER_MULTIPLIER_PRICE`
--
ALTER TABLE `MODIFIER_MULTIPLIER_PRICE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ORDER_TYPE`
--
ALTER TABLE `ORDER_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `PACKAGING_UNIT`
--
ALTER TABLE `PACKAGING_UNIT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PAYOUT_REASONS`
--
ALTER TABLE `PAYOUT_REASONS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PAYOUT_RECEPIENTS`
--
ALTER TABLE `PAYOUT_RECEPIENTS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PIZZA_CRUST`
--
ALTER TABLE `PIZZA_CRUST`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `PIZZA_MODIFIER_PRICE`
--
ALTER TABLE `PIZZA_MODIFIER_PRICE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PIZZA_PRICE`
--
ALTER TABLE `PIZZA_PRICE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PRINTER_GROUP`
--
ALTER TABLE `PRINTER_GROUP`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PURCHASE_ORDER`
--
ALTER TABLE `PURCHASE_ORDER`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `RECEPIE`
--
ALTER TABLE `RECEPIE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `RECEPIE_ITEM`
--
ALTER TABLE `RECEPIE_ITEM`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SHIFT`
--
ALTER TABLE `SHIFT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `SHOP_FLOOR`
--
ALTER TABLE `SHOP_FLOOR`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SHOP_FLOOR_TEMPLATE`
--
ALTER TABLE `SHOP_FLOOR_TEMPLATE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SHOP_TABLE_TYPE`
--
ALTER TABLE `SHOP_TABLE_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TABLE_BOOKING_INFO`
--
ALTER TABLE `TABLE_BOOKING_INFO`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TAX`
--
ALTER TABLE `TAX`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `TERMINAL_PRINTERS`
--
ALTER TABLE `TERMINAL_PRINTERS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TICKET`
--
ALTER TABLE `TICKET`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `TICKET_DISCOUNT`
--
ALTER TABLE `TICKET_DISCOUNT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TICKET_ITEM`
--
ALTER TABLE `TICKET_ITEM`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `TICKET_ITEM_DISCOUNT`
--
ALTER TABLE `TICKET_ITEM_DISCOUNT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TICKET_ITEM_MODIFIER`
--
ALTER TABLE `TICKET_ITEM_MODIFIER`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TRANSACTIONS`
--
ALTER TABLE `TRANSACTIONS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `USERS`
--
ALTER TABLE `USERS`
  MODIFY `AUTO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `USER_TYPE`
--
ALTER TABLE `USER_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `VIRTUAL_PRINTER`
--
ALTER TABLE `VIRTUAL_PRINTER`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `VOID_REASONS`
--
ALTER TABLE `VOID_REASONS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ZIP_CODE_VS_DELIVERY_CHARGE`
--
ALTER TABLE `ZIP_CODE_VS_DELIVERY_CHARGE`
  MODIFY `AUTO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ACTION_HISTORY`
--
ALTER TABLE `ACTION_HISTORY`
  ADD CONSTRAINT `FK3F3AF36B3E20AD51` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `ATTENDENCE_HISTORY`
--
ALTER TABLE `ATTENDENCE_HISTORY`
  ADD CONSTRAINT `FKDFE829A2AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FKDFE829A3E20AD51` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`AUTO_ID`),
  ADD CONSTRAINT `FKDFE829A7660A5E3` FOREIGN KEY (`SHIFT_ID`) REFERENCES `SHIFT` (`ID`);

--
-- Constraints for table `CASH_DRAWER`
--
ALTER TABLE `CASH_DRAWER`
  ADD CONSTRAINT `FK6221077D2AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`);

--
-- Constraints for table `CASH_DRAWER_RESET_HISTORY`
--
ALTER TABLE `CASH_DRAWER_RESET_HISTORY`
  ADD CONSTRAINT `FK719418223E20AD51` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `CURRENCY_BALANCE`
--
ALTER TABLE `CURRENCY_BALANCE`
  ADD CONSTRAINT `FK2CC0E08E28DD6C11` FOREIGN KEY (`CURRENCY_ID`) REFERENCES `CURRENCY` (`ID`),
  ADD CONSTRAINT `FK2CC0E08E9006558` FOREIGN KEY (`CASH_DRAWER_ID`) REFERENCES `CASH_DRAWER` (`ID`),
  ADD CONSTRAINT `FK2CC0E08EFB910735` FOREIGN KEY (`DPR_ID`) REFERENCES `DRAWER_PULL_REPORT` (`ID`);

--
-- Constraints for table `CUSTOMER_PROPERTIES`
--
ALTER TABLE `CUSTOMER_PROPERTIES`
  ADD CONSTRAINT `FKD43068347BBCCF0` FOREIGN KEY (`id`) REFERENCES `CUSTOMER` (`AUTO_ID`);

--
-- Constraints for table `DELIVERY_ADDRESS`
--
ALTER TABLE `DELIVERY_ADDRESS`
  ADD CONSTRAINT `FK29ACA6899E1C3CF1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `CUSTOMER` (`AUTO_ID`);

--
-- Constraints for table `DELIVERY_INSTRUCTION`
--
ALTER TABLE `DELIVERY_INSTRUCTION`
  ADD CONSTRAINT `FK29D9CA39E1C3D97` FOREIGN KEY (`CUSTOMER_NO`) REFERENCES `CUSTOMER` (`AUTO_ID`);

--
-- Constraints for table `DRAWER_ASSIGNED_HISTORY`
--
ALTER TABLE `DRAWER_ASSIGNED_HISTORY`
  ADD CONSTRAINT `FK5A823C91F1DD782B` FOREIGN KEY (`A_USER`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `DRAWER_PULL_REPORT`
--
ALTER TABLE `DRAWER_PULL_REPORT`
  ADD CONSTRAINT `FKAEC362202AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FKAEC362203E20AD51` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `DRAWER_PULL_REPORT_VOIDTICKETS`
--
ALTER TABLE `DRAWER_PULL_REPORT_VOIDTICKETS`
  ADD CONSTRAINT `FK98CF9B143EF4CD9B` FOREIGN KEY (`DPREPORT_ID`) REFERENCES `DRAWER_PULL_REPORT` (`ID`);

--
-- Constraints for table `EMPLOYEE_IN_OUT_HISTORY`
--
ALTER TABLE `EMPLOYEE_IN_OUT_HISTORY`
  ADD CONSTRAINT `FK6D5DB9FA2AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FK6D5DB9FA3E20AD51` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`AUTO_ID`),
  ADD CONSTRAINT `FK6D5DB9FA7660A5E3` FOREIGN KEY (`SHIFT_ID`) REFERENCES `SHIFT` (`ID`);

--
-- Constraints for table `GRATUITY`
--
ALTER TABLE `GRATUITY`
  ADD CONSTRAINT `FK34E4E3771DF2D7F1` FOREIGN KEY (`TICKET_ID`) REFERENCES `TICKET` (`ID`),
  ADD CONSTRAINT `FK34E4E3772AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FK34E4E377AA075D69` FOREIGN KEY (`OWNER_ID`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `INVENTORY_ITEM`
--
ALTER TABLE `INVENTORY_ITEM`
  ADD CONSTRAINT `FK7DC968362CD583C1` FOREIGN KEY (`ITEM_GROUP_ID`) REFERENCES `INVENTORY_GROUP` (`ID`),
  ADD CONSTRAINT `FK7DC968363525E956` FOREIGN KEY (`PUNIT_ID`) REFERENCES `PACKAGING_UNIT` (`ID`),
  ADD CONSTRAINT `FK7DC968366848D615` FOREIGN KEY (`RECIPE_UNIT_ID`) REFERENCES `PACKAGING_UNIT` (`ID`),
  ADD CONSTRAINT `FK7DC9683695E455D3` FOREIGN KEY (`ITEM_LOCATION_ID`) REFERENCES `INVENTORY_LOCATION` (`ID`),
  ADD CONSTRAINT `FK7DC968369E60C333` FOREIGN KEY (`ITEM_VENDOR_ID`) REFERENCES `INVENTORY_VENDOR` (`ID`);

--
-- Constraints for table `INVENTORY_LOCATION`
--
ALTER TABLE `INVENTORY_LOCATION`
  ADD CONSTRAINT `FK59073B58C46A9C15` FOREIGN KEY (`WAREHOUSE_ID`) REFERENCES `INVENTORY_WAREHOUSE` (`ID`);

--
-- Constraints for table `INVENTORY_TRANSACTION`
--
ALTER TABLE `INVENTORY_TRANSACTION`
  ADD CONSTRAINT `FKAF48F43B5B397C5` FOREIGN KEY (`REFERENCE_ID`) REFERENCES `PURCHASE_ORDER` (`ID`),
  ADD CONSTRAINT `FKAF48F43B96A3D6BF` FOREIGN KEY (`ITEM_ID`) REFERENCES `INVENTORY_ITEM` (`ID`),
  ADD CONSTRAINT `FKAF48F43BD152C95F` FOREIGN KEY (`VENDOR_ID`) REFERENCES `INVENTORY_VENDOR` (`ID`),
  ADD CONSTRAINT `FKAF48F43BEDA09759` FOREIGN KEY (`TO_WAREHOUSE_ID`) REFERENCES `INVENTORY_WAREHOUSE` (`ID`),
  ADD CONSTRAINT `FKAF48F43BFF3F328A` FOREIGN KEY (`FROM_WAREHOUSE_ID`) REFERENCES `INVENTORY_WAREHOUSE` (`ID`);

--
-- Constraints for table `ITEM_ORDER_TYPE`
--
ALTER TABLE `ITEM_ORDER_TYPE`
  ADD CONSTRAINT `FKE2B846573AC1D2E0` FOREIGN KEY (`ORDER_TYPE_ID`) REFERENCES `ORDER_TYPE` (`ID`),
  ADD CONSTRAINT `FKE2B8465789FE23F0` FOREIGN KEY (`MENU_ITEM_ID`) REFERENCES `MENU_ITEM` (`ID`);

--
-- Constraints for table `KITCHEN_TICKET`
--
ALTER TABLE `KITCHEN_TICKET`
  ADD CONSTRAINT `FK341CBC275CF1375F` FOREIGN KEY (`PG_ID`) REFERENCES `PRINTER_GROUP` (`ID`);

--
-- Constraints for table `KITCHEN_TICKET_ITEM`
--
ALTER TABLE `KITCHEN_TICKET_ITEM`
  ADD CONSTRAINT `FK1462F02BCB07FAA3` FOREIGN KEY (`KITHEN_TICKET_ID`) REFERENCES `KITCHEN_TICKET` (`ID`);

--
-- Constraints for table `KIT_TICKET_TABLE_NUM`
--
ALTER TABLE `KIT_TICKET_TABLE_NUM`
  ADD CONSTRAINT `FK5696584BB73E273E` FOREIGN KEY (`kit_ticket_id`) REFERENCES `KITCHEN_TICKET` (`ID`);

--
-- Constraints for table `MENUCATEGORY_DISCOUNT`
--
ALTER TABLE `MENUCATEGORY_DISCOUNT`
  ADD CONSTRAINT `FK4F8523E38D9EA931` FOREIGN KEY (`MENUCATEGORY_ID`) REFERENCES `MENU_CATEGORY` (`ID`),
  ADD CONSTRAINT `FK4F8523E3D3E91E11` FOREIGN KEY (`DISCOUNT_ID`) REFERENCES `COUPON_AND_DISCOUNT` (`ID`);

--
-- Constraints for table `MENUGROUP_DISCOUNT`
--
ALTER TABLE `MENUGROUP_DISCOUNT`
  ADD CONSTRAINT `FKE3790E40113BF083` FOREIGN KEY (`MENUGROUP_ID`) REFERENCES `MENU_GROUP` (`ID`),
  ADD CONSTRAINT `FKE3790E40D3E91E11` FOREIGN KEY (`DISCOUNT_ID`) REFERENCES `COUPON_AND_DISCOUNT` (`ID`);

--
-- Constraints for table `MENUITEM_DISCOUNT`
--
ALTER TABLE `MENUITEM_DISCOUNT`
  ADD CONSTRAINT `FKD89CCDEE33662891` FOREIGN KEY (`MENUITEM_ID`) REFERENCES `MENU_ITEM` (`ID`),
  ADD CONSTRAINT `FKD89CCDEED3E91E11` FOREIGN KEY (`DISCOUNT_ID`) REFERENCES `COUPON_AND_DISCOUNT` (`ID`);

--
-- Constraints for table `MENUITEM_MODIFIERGROUP`
--
ALTER TABLE `MENUITEM_MODIFIERGROUP`
  ADD CONSTRAINT `FK312B355B40FDA3C9` FOREIGN KEY (`MODIFIER_GROUP`) REFERENCES `MENU_MODIFIER_GROUP` (`ID`),
  ADD CONSTRAINT `FK312B355B6E7B8B68` FOREIGN KEY (`MENUITEM_MODIFIERGROUP_ID`) REFERENCES `MENU_ITEM` (`ID`);

--
-- Constraints for table `MENUITEM_PIZZAPIRCE`
--
ALTER TABLE `MENUITEM_PIZZAPIRCE`
  ADD CONSTRAINT `FK17BD51A089FE23F0` FOREIGN KEY (`MENU_ITEM_ID`) REFERENCES `MENU_ITEM` (`ID`),
  ADD CONSTRAINT `FK17BD51A0AE5D580` FOREIGN KEY (`PIZZA_PRICE_ID`) REFERENCES `PIZZA_PRICE` (`ID`);

--
-- Constraints for table `MENUITEM_SHIFT`
--
ALTER TABLE `MENUITEM_SHIFT`
  ADD CONSTRAINT `FKE03C92D533662891` FOREIGN KEY (`MENUITEM_ID`) REFERENCES `MENU_ITEM` (`ID`),
  ADD CONSTRAINT `FKE03C92D57660A5E3` FOREIGN KEY (`SHIFT_ID`) REFERENCES `SHIFT` (`ID`);

--
-- Constraints for table `MENUMODIFIER_PIZZAMODIFIERPRICE`
--
ALTER TABLE `MENUMODIFIER_PIZZAMODIFIERPRICE`
  ADD CONSTRAINT `FK572726F374BE2C71` FOREIGN KEY (`PIZZAMODIFIERPRICE_ID`) REFERENCES `PIZZA_MODIFIER_PRICE` (`ID`),
  ADD CONSTRAINT `FK572726F3AE3F2E91` FOREIGN KEY (`MENUMODIFIER_ID`) REFERENCES `MENU_MODIFIER` (`ID`);

--
-- Constraints for table `MENU_GROUP`
--
ALTER TABLE `MENU_GROUP`
  ADD CONSTRAINT `FK4DC1AB7F2E347FF0` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `MENU_CATEGORY` (`ID`);

--
-- Constraints for table `MENU_ITEM`
--
ALTER TABLE `MENU_ITEM`
  ADD CONSTRAINT `FK4CD5A1F35188AA24` FOREIGN KEY (`GROUP_ID`) REFERENCES `MENU_GROUP` (`ID`),
  ADD CONSTRAINT `FK4CD5A1F35CF1375F` FOREIGN KEY (`PG_ID`) REFERENCES `PRINTER_GROUP` (`ID`),
  ADD CONSTRAINT `FK4CD5A1F3A4802F83` FOREIGN KEY (`TAX_ID`) REFERENCES `TAX` (`ID`),
  ADD CONSTRAINT `FK4CD5A1F3F3B77C57` FOREIGN KEY (`RECEPIE`) REFERENCES `RECEPIE` (`ID`);

--
-- Constraints for table `MENU_ITEM_PROPERTIES`
--
ALTER TABLE `MENU_ITEM_PROPERTIES`
  ADD CONSTRAINT `FKF94186FF89FE23F0` FOREIGN KEY (`MENU_ITEM_ID`) REFERENCES `MENU_ITEM` (`ID`);

--
-- Constraints for table `MENU_ITEM_TERMINAL_REF`
--
ALTER TABLE `MENU_ITEM_TERMINAL_REF`
  ADD CONSTRAINT `FK9EA1AFC2AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FK9EA1AFC89FE23F0` FOREIGN KEY (`MENU_ITEM_ID`) REFERENCES `MENU_ITEM` (`ID`);

--
-- Constraints for table `MENU_MODIFIER`
--
ALTER TABLE `MENU_MODIFIER`
  ADD CONSTRAINT `FK59B6B1B75E0C7B8D` FOREIGN KEY (`GROUP_ID`) REFERENCES `MENU_MODIFIER_GROUP` (`ID`),
  ADD CONSTRAINT `FK59B6B1B7A4802F83` FOREIGN KEY (`TAX_ID`) REFERENCES `TAX` (`ID`);

--
-- Constraints for table `MENU_MODIFIER_PROPERTIES`
--
ALTER TABLE `MENU_MODIFIER_PROPERTIES`
  ADD CONSTRAINT `FK1273B4BBB79C6270` FOREIGN KEY (`MENU_MODIFIER_ID`) REFERENCES `MENU_MODIFIER` (`ID`);

--
-- Constraints for table `MODIFIER_MULTIPLIER_PRICE`
--
ALTER TABLE `MODIFIER_MULTIPLIER_PRICE`
  ADD CONSTRAINT `FK8A16099391D62C51` FOREIGN KEY (`MULTIPLIER_ID`) REFERENCES `MULTIPLIER` (`NAME`),
  ADD CONSTRAINT `FK8A1609939C9E4883` FOREIGN KEY (`PIZZA_MODIFIER_PRICE_ID`) REFERENCES `PIZZA_MODIFIER_PRICE` (`ID`),
  ADD CONSTRAINT `FK8A160993AE3F2E91` FOREIGN KEY (`MENUMODIFIER_ID`) REFERENCES `MENU_MODIFIER` (`ID`);

--
-- Constraints for table `PIZZA_MODIFIER_PRICE`
--
ALTER TABLE `PIZZA_MODIFIER_PRICE`
  ADD CONSTRAINT `FKD3DE7E7896183657` FOREIGN KEY (`ITEM_SIZE`) REFERENCES `MENU_ITEM_SIZE` (`ID`);

--
-- Constraints for table `PIZZA_PRICE`
--
ALTER TABLE `PIZZA_PRICE`
  ADD CONSTRAINT `FKEAC112927C59441D` FOREIGN KEY (`CRUST`) REFERENCES `PIZZA_CRUST` (`ID`),
  ADD CONSTRAINT `FKEAC11292A56D141C` FOREIGN KEY (`ORDER_TYPE`) REFERENCES `ORDER_TYPE` (`ID`),
  ADD CONSTRAINT `FKEAC11292DD545B77` FOREIGN KEY (`MENU_ITEM_SIZE`) REFERENCES `MENU_ITEM_SIZE` (`ID`);

--
-- Constraints for table `PRINTER_GROUP_PRINTERS`
--
ALTER TABLE `PRINTER_GROUP_PRINTERS`
  ADD CONSTRAINT `FKC05B805E5F31265C` FOREIGN KEY (`printer_id`) REFERENCES `PRINTER_GROUP` (`ID`);

--
-- Constraints for table `RECEPIE`
--
ALTER TABLE `RECEPIE`
  ADD CONSTRAINT `FK6B4E177764931EFC` FOREIGN KEY (`MENU_ITEM`) REFERENCES `MENU_ITEM` (`ID`);

--
-- Constraints for table `RECEPIE_ITEM`
--
ALTER TABLE `RECEPIE_ITEM`
  ADD CONSTRAINT `FK855626DB1682B10E` FOREIGN KEY (`INVENTORY_ITEM`) REFERENCES `INVENTORY_ITEM` (`ID`),
  ADD CONSTRAINT `FK855626DBCAE89B83` FOREIGN KEY (`RECEPIE_ID`) REFERENCES `RECEPIE` (`ID`);

--
-- Constraints for table `SHOP_FLOOR_TEMPLATE`
--
ALTER TABLE `SHOP_FLOOR_TEMPLATE`
  ADD CONSTRAINT `FKBA6EFBD68979C3CD` FOREIGN KEY (`FLOOR_ID`) REFERENCES `SHOP_FLOOR` (`ID`);

--
-- Constraints for table `SHOP_FLOOR_TEMPLATE_PROPERTIES`
--
ALTER TABLE `SHOP_FLOOR_TEMPLATE_PROPERTIES`
  ADD CONSTRAINT `FKD70C313CA36AB054` FOREIGN KEY (`id`) REFERENCES `SHOP_FLOOR_TEMPLATE` (`ID`);

--
-- Constraints for table `SHOP_TABLE`
--
ALTER TABLE `SHOP_TABLE`
  ADD CONSTRAINT `FK2458E9258979C3CD` FOREIGN KEY (`FLOOR_ID`) REFERENCES `SHOP_FLOOR` (`ID`);

--
-- Constraints for table `TABLE_BOOKING_INFO`
--
ALTER TABLE `TABLE_BOOKING_INFO`
  ADD CONSTRAINT `FK301C4DE53E20AD51` FOREIGN KEY (`user_id`) REFERENCES `USERS` (`AUTO_ID`),
  ADD CONSTRAINT `FK301C4DE59E1C3CF1` FOREIGN KEY (`customer_id`) REFERENCES `CUSTOMER` (`AUTO_ID`);

--
-- Constraints for table `TABLE_BOOKING_MAPPING`
--
ALTER TABLE `TABLE_BOOKING_MAPPING`
  ADD CONSTRAINT `FK6BC51417160DE3B1` FOREIGN KEY (`BOOKING_ID`) REFERENCES `TABLE_BOOKING_INFO` (`ID`),
  ADD CONSTRAINT `FK6BC51417DC46948D` FOREIGN KEY (`TABLE_ID`) REFERENCES `SHOP_TABLE` (`ID`);

--
-- Constraints for table `TABLE_TYPE_RELATION`
--
ALTER TABLE `TABLE_TYPE_RELATION`
  ADD CONSTRAINT `FK93802290DC46948D` FOREIGN KEY (`TABLE_ID`) REFERENCES `SHOP_TABLE` (`ID`),
  ADD CONSTRAINT `FK93802290F5D6E47B` FOREIGN KEY (`TYPE_ID`) REFERENCES `SHOP_TABLE_TYPE` (`ID`);

--
-- Constraints for table `TERMINAL`
--
ALTER TABLE `TERMINAL`
  ADD CONSTRAINT `FKE83D827C969C6DE` FOREIGN KEY (`ASSIGNED_USER`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `TERMINAL_PRINTERS`
--
ALTER TABLE `TERMINAL_PRINTERS`
  ADD CONSTRAINT `FK99EDE5FC2AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FK99EDE5FCC433E65A` FOREIGN KEY (`VIRTUAL_PRINTER_ID`) REFERENCES `VIRTUAL_PRINTER` (`ID`);

--
-- Constraints for table `TICKET`
--
ALTER TABLE `TICKET`
  ADD CONSTRAINT `FK937B5F0C1F6A9A4A` FOREIGN KEY (`VOID_BY_USER`) REFERENCES `USERS` (`AUTO_ID`),
  ADD CONSTRAINT `FK937B5F0C2AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FK937B5F0C7660A5E3` FOREIGN KEY (`SHIFT_ID`) REFERENCES `SHIFT` (`ID`),
  ADD CONSTRAINT `FK937B5F0CAA075D69` FOREIGN KEY (`OWNER_ID`) REFERENCES `USERS` (`AUTO_ID`),
  ADD CONSTRAINT `FK937B5F0CC188EA51` FOREIGN KEY (`GRATUITY_ID`) REFERENCES `GRATUITY` (`ID`),
  ADD CONSTRAINT `FK937B5F0CF575C7D4` FOREIGN KEY (`DRIVER_ID`) REFERENCES `USERS` (`AUTO_ID`);

--
-- Constraints for table `TICKET_DISCOUNT`
--
ALTER TABLE `TICKET_DISCOUNT`
  ADD CONSTRAINT `FK1FA465141DF2D7F1` FOREIGN KEY (`TICKET_ID`) REFERENCES `TICKET` (`ID`);

--
-- Constraints for table `TICKET_ITEM`
--
ALTER TABLE `TICKET_ITEM`
  ADD CONSTRAINT `FK979F54661DF2D7F1` FOREIGN KEY (`TICKET_ID`) REFERENCES `TICKET` (`ID`),
  ADD CONSTRAINT `FK979F546633E5D3B2` FOREIGN KEY (`SIZE_MODIFIER_ID`) REFERENCES `TICKET_ITEM_MODIFIER` (`ID`),
  ADD CONSTRAINT `FK979F54665CF1375F` FOREIGN KEY (`PG_ID`) REFERENCES `PRINTER_GROUP` (`ID`);

--
-- Constraints for table `TICKET_ITEM_ADDON_RELATION`
--
ALTER TABLE `TICKET_ITEM_ADDON_RELATION`
  ADD CONSTRAINT `FK9F1996346C108EF0` FOREIGN KEY (`MODIFIER_ID`) REFERENCES `TICKET_ITEM_MODIFIER` (`ID`),
  ADD CONSTRAINT `FK9F199634DEC6120A` FOREIGN KEY (`TICKET_ITEM_ID`) REFERENCES `TICKET_ITEM` (`ID`);

--
-- Constraints for table `TICKET_ITEM_COOKING_INSTRUCTION`
--
ALTER TABLE `TICKET_ITEM_COOKING_INSTRUCTION`
  ADD CONSTRAINT `FK3825F9D0DEC6120A` FOREIGN KEY (`TICKET_ITEM_ID`) REFERENCES `TICKET_ITEM` (`ID`);

--
-- Constraints for table `TICKET_ITEM_DISCOUNT`
--
ALTER TABLE `TICKET_ITEM_DISCOUNT`
  ADD CONSTRAINT `FK3DF5D4FAB9276E77` FOREIGN KEY (`TICKET_ITEMID`) REFERENCES `TICKET_ITEM` (`ID`);

--
-- Constraints for table `TICKET_ITEM_MODIFIER`
--
ALTER TABLE `TICKET_ITEM_MODIFIER`
  ADD CONSTRAINT `FK8FD6290DEC6120A` FOREIGN KEY (`TICKET_ITEM_ID`) REFERENCES `TICKET_ITEM` (`ID`);

--
-- Constraints for table `TICKET_ITEM_MODIFIER_RELATION`
--
ALTER TABLE `TICKET_ITEM_MODIFIER_RELATION`
  ADD CONSTRAINT `FK5D3F9ACB6C108EF0` FOREIGN KEY (`MODIFIER_ID`) REFERENCES `TICKET_ITEM_MODIFIER` (`ID`),
  ADD CONSTRAINT `FK5D3F9ACBDEC6120A` FOREIGN KEY (`TICKET_ITEM_ID`) REFERENCES `TICKET_ITEM` (`ID`);

--
-- Constraints for table `TICKET_PROPERTIES`
--
ALTER TABLE `TICKET_PROPERTIES`
  ADD CONSTRAINT `FK70ECD046223049DE` FOREIGN KEY (`id`) REFERENCES `TICKET` (`ID`);

--
-- Constraints for table `TICKET_TABLE_NUM`
--
ALTER TABLE `TICKET_TABLE_NUM`
  ADD CONSTRAINT `FK65AF15E21DF2D7F1` FOREIGN KEY (`ticket_id`) REFERENCES `TICKET` (`ID`);

--
-- Constraints for table `TRANSACTIONS`
--
ALTER TABLE `TRANSACTIONS`
  ADD CONSTRAINT `FKFE9871551DF2D7F1` FOREIGN KEY (`TICKET_ID`) REFERENCES `TICKET` (`ID`),
  ADD CONSTRAINT `FKFE9871552AD2D031` FOREIGN KEY (`TERMINAL_ID`) REFERENCES `TERMINAL` (`ID`),
  ADD CONSTRAINT `FKFE9871553E20AD51` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`AUTO_ID`),
  ADD CONSTRAINT `FKFE987155CA43B6` FOREIGN KEY (`PAYOUT_RECEPIENT_ID`) REFERENCES `PAYOUT_RECEPIENTS` (`ID`),
  ADD CONSTRAINT `FKFE987155FC697D9E` FOREIGN KEY (`PAYOUT_REASON_ID`) REFERENCES `PAYOUT_REASONS` (`ID`);

--
-- Constraints for table `TRANSACTION_PROPERTIES`
--
ALTER TABLE `TRANSACTION_PROPERTIES`
  ADD CONSTRAINT `FKE3DE65548E8203BC` FOREIGN KEY (`id`) REFERENCES `TRANSACTIONS` (`ID`);

--
-- Constraints for table `USERS`
--
ALTER TABLE `USERS`
  ADD CONSTRAINT `FK4D495E87660A5E3` FOREIGN KEY (`SHIFT_ID`) REFERENCES `SHIFT` (`ID`),
  ADD CONSTRAINT `FK4D495E8897B1E39` FOREIGN KEY (`N_USER_TYPE`) REFERENCES `USER_TYPE` (`ID`),
  ADD CONSTRAINT `FK4D495E8D9409968` FOREIGN KEY (`currentTerminal`) REFERENCES `TERMINAL` (`ID`);

--
-- Constraints for table `USER_USER_PERMISSION`
--
ALTER TABLE `USER_USER_PERMISSION`
  ADD CONSTRAINT `FK2DBEAA4F283ECC6` FOREIGN KEY (`permissionId`) REFERENCES `USER_TYPE` (`ID`),
  ADD CONSTRAINT `FK2DBEAA4F8F23F5E` FOREIGN KEY (`elt`) REFERENCES `USER_PERMISSION` (`NAME`);

--
-- Constraints for table `VIRTUALPRINTER_ORDER_TYPE`
--
ALTER TABLE `VIRTUALPRINTER_ORDER_TYPE`
  ADD CONSTRAINT `FK9AF7853BCF15F4A6` FOREIGN KEY (`printer_id`) REFERENCES `VIRTUAL_PRINTER` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
